﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BackendServer.Core.Entities
{
    public class Worker : DatabaseObject
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Rank { get; set; }
        public DateTime WorkingFrom { get; set; }

        public Photo ProfilePhoto { get; set; }
        public int? ProfilePhotoId { get; set; }
    }
}
