﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BackendServer.Core.Entities
{
    [Table("Users")]
    public class User : DatabaseObject
    {
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public Permission PermissionLevel { get; set; }
    }
}
