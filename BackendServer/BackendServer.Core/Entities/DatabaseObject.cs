﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BackendServer.Core.Entities
{
    public abstract class DatabaseObject
    {
        [Required]
        public int Id { get; set; }
    }
}
