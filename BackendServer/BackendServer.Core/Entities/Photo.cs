﻿using BackendServer.Core.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendServer.Core.Entities
{
    [Table("Photos")]
    public class Photo : DatabaseObject
    {
        public string Path { get; set; }
        public string ThumbnailPath { get; set; }
        public string Comment { get; set; }
        public string PhotoTarget { get; set; }
        public string PublicId { get; set; }
        
        // koncepcja wynikająca z EF
        public PhotoAlbum PhotoAlbum { get; set; }
        public int? PhotoAlbumId { get;set; }

        public Worker Worker { get; set; }

        public EmploymentOffer EmploymentOffer { get; set; }

        public Offer Offer { get; set; }
    }
}
