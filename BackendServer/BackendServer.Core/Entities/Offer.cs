﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BackendServer.Core.Entities
{
    [Table("Offers")]
    public class Offer : DatabaseObject
    {
        public int OrderNumber { get; set; }
        public string Title { get; set; }

        public Photo Image { get; set; }
        public int? ImageId { get; set; }

        public HtmlInput HtmlInput { get; set; }
        public int? HtmlInputId { get; set; }
    }
}
