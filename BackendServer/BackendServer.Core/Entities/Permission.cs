﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BackendServer.Core.Entities
{
    [Table("Permissions")]
    public class Permission : DatabaseObject
    {
        public string PermissionName { get; set; }
    }
}
