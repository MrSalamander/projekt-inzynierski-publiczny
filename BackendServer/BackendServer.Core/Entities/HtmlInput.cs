﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BackendServer.Core.Entities
{
    [Table("HtmlInputs")]
    public class HtmlInput : DatabaseObject
    {
        [Required]
        public string Value { get; set; }

        [Required]
        public string Target { get; set; }

        public EmploymentOffer EmploymentOffer { get; set; }

        public Offer Offer { get; set; }
        
    }
}
