﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendServer.Core.Entities
{
    public class ContactParam : DatabaseObject
    {
        public string ParamType { get; set; }
        public string ParamValue { get; set; }
    }
}
