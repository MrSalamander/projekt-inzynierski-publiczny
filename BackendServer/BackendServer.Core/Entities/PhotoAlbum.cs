﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BackendServer.Core.Entities
{
    [Table("PhotoAlbums")]
    public class PhotoAlbum : DatabaseObject
    {
        [StringLength(50)]
        public string Name { get; set; }
        
        [StringLength(2000)]
        public string Comment { get; set; }

        public ICollection<Photo> Photos { get; set; }
    }
}
