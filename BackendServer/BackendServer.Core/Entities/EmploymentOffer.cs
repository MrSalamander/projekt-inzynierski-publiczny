﻿using BackendServer.Core.DTOs.Photos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BackendServer.Core.Entities
{
    [Table("EmploymentOffers")]
    public class EmploymentOffer : DatabaseObject
    {
        public HtmlInput HtmlInput { get; set; }
        public int? HtmlInputId { get; set; }

        public Photo Photo { get; set; }
        public int? PhotoId { get; set; }
    }
}
