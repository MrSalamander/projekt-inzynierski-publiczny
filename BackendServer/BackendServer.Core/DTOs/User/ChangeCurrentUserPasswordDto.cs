﻿
namespace BackendServer.Core.DTOs.User
{
    public class ChangeCurrentUserPasswordDto
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
