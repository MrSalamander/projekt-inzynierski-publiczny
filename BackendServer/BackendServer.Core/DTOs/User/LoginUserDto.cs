﻿using System.ComponentModel.DataAnnotations;

namespace BackendServer.Core.DTOs.User
{
    public class LoginUserDto
    {
        public LoginUserDto(string username, string password)
        {
            Username = username;
            Password = password;
        }

        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(32, MinimumLength = 4)]
        public string Password { get; set; }

    }
}
