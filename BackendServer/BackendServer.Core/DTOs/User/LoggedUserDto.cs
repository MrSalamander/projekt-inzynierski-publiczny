﻿
namespace BackendServer.Core.DTOs.User
{
    public class LoggedUserDto
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string PermissionLevelName { get; set; }
    }
}
