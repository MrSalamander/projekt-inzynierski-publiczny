﻿using System.ComponentModel.DataAnnotations;

namespace BackendServer.Core.DTOs.User
{
    public class RegisterUserDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(32, MinimumLength = 4)]
        public string Password { get; set; }

        [Required]
        public string PermissionName { get; set; }

    }
}
