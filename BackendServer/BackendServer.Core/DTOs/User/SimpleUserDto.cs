﻿
namespace BackendServer.Core.DTOs.User
{
    public class SimpleUserDto
    {
        public string Username { get; set; }
        public string PermissionLevelName { get; set; }
    }
}
