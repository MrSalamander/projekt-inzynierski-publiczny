﻿using BackendServer.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendServer.Core.DTOs
{
    public class EmploymentOfferDto
    {
        public int Id { get; set; }
        public int? PhotoId { get; set; }
        public HtmlInput HtmlInput { get; set; }
    }
}
