﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace BackendServer.Core.DTOs
{
    public class WorkerInputDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Rank { get; set; }
        
        public int Id { get; set; }

        public DateTime WorkingFrom { get; set; }

        public IFormFile Photo { get; set; }
    }
}
