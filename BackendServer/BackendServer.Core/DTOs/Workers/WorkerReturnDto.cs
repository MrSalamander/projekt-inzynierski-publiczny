﻿using BackendServer.Core.DTOs.Photos;
using System;

namespace BackendServer.Core.DTOs.Workers
{
    public class WorkerReturnDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Rank { get; set; }
        public DateTime WorkingFrom { get; set; }
        public PhotoDto ProfilePhoto { get; set; }
    }
}
