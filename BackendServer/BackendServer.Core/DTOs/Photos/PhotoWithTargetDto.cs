﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendServer.Core.DTOs.Photos
{
    public class PhotoWithTargetDto : PhotoDto
    {
        public string PhotoTarget { get; set; }
    }
}
