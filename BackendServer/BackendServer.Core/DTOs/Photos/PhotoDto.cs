﻿
namespace BackendServer.Core.DTOs.Photos
{
    public class PhotoDto
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public string Path { get; set; }
    }
}
