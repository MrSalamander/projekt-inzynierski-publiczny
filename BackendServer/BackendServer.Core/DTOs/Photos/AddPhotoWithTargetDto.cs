﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendServer.Core.DTOs.Photos
{
    public class AddPhotoWithTargetDto
    {
        public IFormFile File { get; set; }
        public string Comment { get; set; }
        public string Target { get; set; }
        public int TargetWidth { get; set; }
    }
}
