﻿
namespace BackendServer.Core.DTOs.Photos
{
    public class PhotoFromAlbumDto : PhotoDto
    {
        public int PhotoAlbumId { get; set; }
        public string ThumbnailPath { get; set; }
    }
}
