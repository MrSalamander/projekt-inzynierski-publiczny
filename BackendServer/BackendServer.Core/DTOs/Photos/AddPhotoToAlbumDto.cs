﻿using Microsoft.AspNetCore.Http;

namespace BackendServer.Core.DTOs.Photos
{
    public class AddPhotoToAlbumDto
    {
        public IFormFile File{ get; set; }
        public string Comment { get; set; }
        public int AlbumId { get; set; }
    }
}
