﻿using System.ComponentModel.DataAnnotations;

namespace BackendServer.Core.DTOs.Albums
{
    public class PhotoAlbumAddDto
    {
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(2000)]
        public string Comment { get; set; }
    }
}
