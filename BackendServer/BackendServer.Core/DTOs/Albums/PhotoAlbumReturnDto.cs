﻿using BackendServer.Core.DTOs.Photos;
using System.Collections.Generic;

namespace BackendServer.Core.DTOs.Albums
{
    public class PhotoAlbumReturnDto : PhotoAlbumAddDto
    {
        public int Id { get; set; }
        public ICollection<PhotoFromAlbumDto> Photos{ get; set; }
    }
}
