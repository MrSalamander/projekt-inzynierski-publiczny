﻿
namespace BackendServer.Core.DTOs.Albums
{
    public class PhotoAlbumUpdateDto : PhotoAlbumAddDto
    {
        public int Id { get; set; }
    }
}
