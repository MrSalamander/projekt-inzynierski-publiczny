﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BackendServer.Core.Interfaces
{
    public interface IPhotoService
    {
        Task<KeyValuePair<int, string>> DeletePhoto(int id);
    }
}
