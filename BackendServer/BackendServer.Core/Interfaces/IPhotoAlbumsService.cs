﻿using BackendServer.Core.Entities;
using BackendServer.Core.DTOs.Albums;
using System.Threading.Tasks;

namespace BackendServer.Core.Services.Interfaces
{
    public interface IPhotoAlbumsService
    {
        Task<PhotoAlbum> AddAlbum(PhotoAlbumAddDto photoAlbumDto);
        Task<PhotoAlbum> EditAlbum(PhotoAlbumUpdateDto photoAlbumUpdateDto);
    }
}
