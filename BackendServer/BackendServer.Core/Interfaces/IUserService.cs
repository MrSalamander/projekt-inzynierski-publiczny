﻿using BackendServer.Core.DTOs.User;
using System.Threading.Tasks;

namespace BackendServer.Core.Services.Interfaces
{
    public interface IUserService
    {
        Task<bool> UserExists(string userName);
        Task<bool> PasswordMatches(LoginUserDto loginDto);
        LoggedUserDto CreateUserDtoWithToken(LoginUserDto loginDto);
        void RegisterUser(RegisterUserDto newUser);
        void UpdateUsersPassword(string username, string newPassword);
        Task UpdateUsersPermission(SimpleUserDto simpleUserDto);
    }
}
