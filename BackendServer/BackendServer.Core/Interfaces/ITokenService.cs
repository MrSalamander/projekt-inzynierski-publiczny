﻿using BackendServer.Core.Entities;

namespace BackendServer.Core.Services.Interfaces
{
    public interface ITokenService
    {
        string CreateToken(User user);
    }
}
