﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendServer.Core.Enums
{
    public enum PhotoPurposeEnum
    {
        Gallery = 1,
        Newsletter = 2,
        Worker = 3
    }
}
