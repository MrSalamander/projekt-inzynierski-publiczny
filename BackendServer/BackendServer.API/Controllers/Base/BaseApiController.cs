﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseApiController : ControllerBase
    {
        public ActionResult ParseHttpCodeToMethod(KeyValuePair<int, string> result)
        {
            switch (result.Key)
            {
                case 200:
                    return Ok(result.Value);
                case 404:
                    return NotFound(result.Value);
                case 400:
                    return BadRequest(result.Value);
                default:
                    return BadRequest(result.Value);
            }
        }
    }
}
