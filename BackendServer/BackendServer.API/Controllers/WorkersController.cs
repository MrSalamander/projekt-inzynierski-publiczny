﻿using AutoMapper;
using BackendServer.API.Entities;
using BackendServer.Core.Entities;
using BackendServer.Core.DTOs;
using BackendServer.Core.DTOs.Workers;
using BackendServer.Infrastructure.ExternalApi.Cloudinary;
using BackendServer.Infrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BackendServer.Core.Interfaces;

namespace BackendServer.API.Controllers
{
    [Authorize(Roles = PermissionConstRoles.Admin +","+ PermissionConstRoles.StarszyEdytor)]
    public class WorkersController : BaseApiController
    {
        private readonly IWorkersRepository _workersRepository;
        private readonly ISimpleEntityRepository<Photo> _photoRepository;
        private readonly ICloudinaryPhotoService _cloudinaryPhotoService;
        private readonly IPhotoService _photoService;
        private readonly IMapper _mapper;

        #region Ctor
        public WorkersController(
            IWorkersRepository workersRepository, 
            ISimpleEntityRepository<Photo> photoRepository, 
            ICloudinaryPhotoService cloudinaryPhotoService,
            IPhotoService photoService,
            IMapper mapper)
        {
            this._workersRepository = workersRepository;
            this._photoRepository = photoRepository;
            this._cloudinaryPhotoService = cloudinaryPhotoService;
            this._photoService = photoService;
            this._mapper = mapper;
        }
        #endregion

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<WorkerReturnDto>>> GetAllWorkers() => Ok(_mapper.Map<IEnumerable<WorkerReturnDto>>(await _workersRepository.GetAllAsync()));

        [HttpGet("id", Name = "GetWorker")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<WorkerReturnDto>>> GetAllWorkers(int id) => Ok(_mapper.Map<WorkerReturnDto>( await _workersRepository.GetByIdAsync(id)));

        [HttpPost("Add")]
        public async Task<ActionResult<WorkerReturnDto>> AddWorker([FromForm] WorkerInputDto workerAddDto)
        {
            Worker newWorker = _mapper.Map<WorkerInputDto, Worker>(workerAddDto);

            if (workerAddDto.Photo != null)
            {
                await using var stream = workerAddDto.Photo.OpenReadStream();
                var uploadRes = await _cloudinaryPhotoService.AddPhotoAsync(stream, workerAddDto.Photo.FileName, 340, true);
                if(uploadRes.Error != null)
                    return BadRequest("Nie udało się wgrać pliku do magazynu w chmurze." + Environment.NewLine + uploadRes.Error.Message);


                newWorker.ProfilePhoto = new Photo()
                {
                    Path = uploadRes.Url.ToString(),
                    PublicId = uploadRes.PublicId,
                    PhotoTarget = "Worker",
                };
            }

            var id = _workersRepository.Insert(newWorker);
            var newlyCreatedWorker = await _workersRepository.GetByIdAsync(id);

            return CreatedAtRoute("GetWorker", new { id = newlyCreatedWorker.Id }, _mapper.Map<WorkerReturnDto>(newlyCreatedWorker));
        }

        [HttpPost("Update")]
        public async Task<ActionResult<WorkerReturnDto>> UpdateWorker([FromForm] WorkerInputDto worker)
        {
            if (worker.Id == null)
                return BadRequest("Przy aktualizacji wymagane jest pole Id.");
            var currentWorker = await _workersRepository.GetByIdAsync(worker.Id);
            if (currentWorker == null)
                return BadRequest("Nie znaleziono pracownika o id = " + worker.Id);
            MapWorkerWithValidation(worker, currentWorker);

            if (worker.Photo != null)
            {
                await using var stream = worker.Photo.OpenReadStream();
                var uploadRes = await _cloudinaryPhotoService.AddPhotoAsync(stream, worker.Photo.FileName, 340, true);
                if (uploadRes.Error != null)
                    return BadRequest("Nie udało się wgrać pliku do magazynu w chmurze." + Environment.NewLine + uploadRes.Error.Message);

                if (currentWorker.ProfilePhotoId.HasValue)
                {
                    var result = await _photoService.DeletePhoto(currentWorker.ProfilePhotoId.Value);
                    var parsedResult = ParseHttpCodeToMethod(result);
                    if (parsedResult.GetType() != typeof(OkObjectResult))
                        return parsedResult;
                }

                currentWorker.ProfilePhoto = new Photo()
                {
                    PhotoTarget = "Worker",
                    Path = uploadRes.SecureUrl.ToString(),
                    PublicId = uploadRes.PublicId
                };
            }

            _workersRepository.Update(currentWorker);
            return Ok(_mapper.Map<WorkerReturnDto>(currentWorker));
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult> DeleteWorker(int id)
        {
            var worker = await _workersRepository.GetByIdAsync(id);
            if (worker != null && worker.ProfilePhotoId.HasValue)
            {
                var result = await _photoService.DeletePhoto(worker.ProfilePhotoId.Value);
                var parsedResult = ParseHttpCodeToMethod(result);
                if (parsedResult.GetType() != typeof(OkObjectResult))
                    return parsedResult;
            }

            _workersRepository.Delete(id);
            return Ok("Pomyślnie usunięto pracownika");
        }

        [HttpDelete("delete-photo/{id}")]
        public async Task<ActionResult> DeleteWorkerPhoto(int id)
        {
            var worker = await _workersRepository.GetByIdAsync(id);
            if (worker == null)
                return BadRequest("Nie znaleziono pracownika o id = " + id);

            if (!worker.ProfilePhotoId.HasValue)
                return Ok("Pracownik nie ma zdjęcia");

            var result = await _photoService.DeletePhoto(worker.ProfilePhotoId.Value);
            return ParseHttpCodeToMethod(result);
        }

        #region private methods
        private void MapWorkerWithValidation(WorkerInputDto worker, Worker currentWorker)
        {
            // Nie wykorzystano mappera z uwagi na tracking EF
            currentWorker.Name = worker.Name;
            currentWorker.Rank = worker.Rank;
            currentWorker.WorkingFrom = worker.WorkingFrom;
        }
        #endregion
    }
}

