﻿using AutoMapper;
using BackendServer.Core.DTOs;
using BackendServer.Core.Entities;
using BackendServer.Core.Interfaces;
using BackendServer.Infrastructure.ExternalApi.Cloudinary;
using BackendServer.Infrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.API.Controllers
{
    [Authorize]
    public class EmploymentOffersController : BaseApiController
    {
        private readonly ISimpleEntityRepository<Photo> _simplePhotosRepository;
        private readonly IPhotoService _photoService;
        private readonly ISimpleEntityRepository<EmploymentOffer> _simpleEmploymentOffersRepository;
        private readonly ISimpleEntityRepository<HtmlInput> _simpleHtmlInputsRepository;

        private readonly IMapper _mapper;

        #region ctor
        public EmploymentOffersController(
            ICloudinaryPhotoService cloudinaryPhotoService,
            ISimpleEntityRepository<EmploymentOffer> simpleOffersRepository,
            ISimpleEntityRepository<Photo> simplePhotosRepository,
            IPhotoService photoService,
            IMapper mapper, ISimpleEntityRepository<HtmlInput> simpleHtmlInputsRepository)
        {
            _simpleEmploymentOffersRepository = simpleOffersRepository;
            _mapper = mapper;
            _simplePhotosRepository = simplePhotosRepository;
            this._photoService = photoService;
            _simpleHtmlInputsRepository = simpleHtmlInputsRepository;
        }
        #endregion

        [AllowAnonymous]
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<EmploymentOfferDto>>> GetOffers()
        {
            var offerEntities = await _simpleEmploymentOffersRepository.GetAllAsync();
            foreach(var offer in offerEntities)
            {
                if (offer.HtmlInputId.HasValue)
                    offer.HtmlInput = await _simpleHtmlInputsRepository.GetByIdAsync(offer.HtmlInputId.Value);
            }
            var offerDtos = _mapper.Map<ICollection<EmploymentOfferDto>>(offerEntities);

            return Ok(offerDtos);
        }

        [HttpPost()]
        public async Task<ActionResult<EmploymentOfferDto>> AddOffer(EmploymentOfferDto offerDto)
        {
            var entity = _mapper.Map<EmploymentOffer>(offerDto);
            if (offerDto.PhotoId.HasValue && offerDto.PhotoId.Value >= 0)
            {
                var entityPhoto = await _simplePhotosRepository.GetByIdAsync(offerDto.PhotoId.Value);
                entity.Photo = entityPhoto;
            }
            var tmpInput = offerDto.HtmlInput;
            entity.HtmlInput = null;
            entity.HtmlInputId = null;

            var newEntityId = _simpleEmploymentOffersRepository.Insert(entity);
            entity = await _simpleEmploymentOffersRepository.GetByIdAsync(newEntityId);
            entity.HtmlInput = tmpInput;
            entity.HtmlInputId = tmpInput.Id;
            _simpleEmploymentOffersRepository.Update(entity);

            return Ok(_mapper.Map<EmploymentOfferDto>(entity));
        }

        [HttpPost("update")]
        public async Task<ActionResult> UpdateOffer(EmploymentOfferDto offerDto)
        {
            var entity = await _simpleEmploymentOffersRepository.GetByIdAsync(offerDto.Id);
            
            if (offerDto.PhotoId.HasValue)
            {
                if(entity.Photo != null)
                {
                    var result = await _photoService.DeletePhoto(entity.PhotoId.Value);
                    var parsedResult = ParseHttpCodeToMethod(result);
                    if (parsedResult.GetType() != typeof(OkObjectResult))
                        return parsedResult;
                }
                
                var newPhoto = await _simplePhotosRepository.GetByIdAsync(offerDto.PhotoId.Value);
                entity.Photo = newPhoto;
            }

            entity.HtmlInput = offerDto.HtmlInput;
            _simpleEmploymentOffersRepository.Update(entity);
            return Ok("Pomyślnie zaktualizowano ofertę.");
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult> DeleteOffer(int id)
        {
            var entity = await _simpleEmploymentOffersRepository.GetByIdAsync(id);
            if (entity.PhotoId.HasValue)
            {
                var result = await _photoService.DeletePhoto(entity.PhotoId.Value);
                var parsedResult = ParseHttpCodeToMethod(result);
                if (parsedResult.GetType() != typeof(OkObjectResult))
                    return parsedResult;
            }

            if (entity.HtmlInputId.HasValue)
                _simpleHtmlInputsRepository.Delete(entity.HtmlInputId.Value);

            _simpleEmploymentOffersRepository.Delete(id);
            return Ok("Pomyślnie usunięto ofertę");
        }

        [HttpDelete("deleteOfferPhoto/{offerId}")]
        public async Task<ActionResult> DeletePhoto(int offerId)
        {
            var offer = await _simpleEmploymentOffersRepository.GetByIdAsync(offerId);
            if(offer == null)
                return NotFound("Nie znaleziono oferty o id = " + offerId);

            if (!offer.PhotoId.HasValue)
                return Ok("Pracownik nie ma zdjęcia");

            var result = await _photoService.DeletePhoto(offer.PhotoId.Value);
            return ParseHttpCodeToMethod(result);
        }
    }
}
