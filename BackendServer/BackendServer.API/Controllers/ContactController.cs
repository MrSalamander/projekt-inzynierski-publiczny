﻿using BackendServer.Core.Entities;
using BackendServer.Infrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.API.Controllers
{
    [Authorize]
    public class ContactController : BaseApiController
    {
        private readonly ISimpleEntityRepository<ContactParam> _contactParamsRepository;

        public ContactController(ISimpleEntityRepository<ContactParam> contactParamsRepository)
        {
            _contactParamsRepository = contactParamsRepository;
        }

        [HttpGet()]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<ContactParam>>> GetContactParams()
        {
            var contactParams = await _contactParamsRepository.GetAllAsync();
            if (contactParams == null)
                return NoContent();

            return Ok(contactParams);
        }

        [HttpPost]
        public ActionResult<ContactParam> AddContactParam(ContactParam contactParam)
        {
            var contactParamId = _contactParamsRepository.Insert(contactParam);
            contactParam.Id = contactParamId;

            return Ok(contactParam);
        }

        [HttpPost("update")]
        public ActionResult<ContactParam> UpdateContactParam(ContactParam contactParam)
        {
            _contactParamsRepository.Update(contactParam);

            return Ok(contactParam);
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult> DeleteContactParam(int id)
        {
            if (await _contactParamsRepository.GetByIdAsync(id) == null)
                return NotFound($"parametr o id = {id} nie istnieje.");

            _contactParamsRepository.Delete(id);
            return Ok("Pomyslnie usunięto parametr");
        }
    }
}
