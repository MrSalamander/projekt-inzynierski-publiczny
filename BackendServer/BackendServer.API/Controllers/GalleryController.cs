﻿using AutoMapper;
using BackendServer.API.Extensions;
using BackendServer.Core.Entities;
using BackendServer.Core.DTOs.Albums;
using BackendServer.Core.DTOs.Photos;
using BackendServer.Infrastructure.ExternalApi.Cloudinary;
using BackendServer.Infrastructure.Repositories.Interfaces;
using BackendServer.Core.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Linq;
using BackendServer.Core.Interfaces;

namespace BackendServer.API.Controllers
{
    [Authorize]
    public class GalleryController : BaseApiController
    {
        private readonly IPhotoAlbumsService _photoAlbumsService;
        private readonly IPhotoAlbumsRepository _photoAlbumsRepository;
        private readonly ISimpleEntityRepository<Photo> _photosRepository;
        private readonly IPhotoService _photoService;
        private readonly ICloudinaryPhotoService _cloudinaryPhotoService;
        private readonly IMapper _mapper;
        private readonly int _thumbnailWidth;
        private readonly int _fullWidth;

        public GalleryController(
            IPhotoAlbumsService photosService, 
            IPhotoAlbumsRepository photoAlbumsRepository, 
            ISimpleEntityRepository<Photo> photosRepository,
            IPhotoService photoService,
            ICloudinaryPhotoService cloudinaryPhotoService,
            IMapper mapper,
            IConfiguration config)
        {
            _photoAlbumsService = photosService;
            _photoAlbumsRepository = photoAlbumsRepository;
            _photosRepository = photosRepository;
            this._photoService = photoService;
            this._cloudinaryPhotoService = cloudinaryPhotoService;
            this._mapper = mapper;

            this._thumbnailWidth = int.Parse(config["PhotosTransitionSettings:ThumbnailWidth"]);
            this._fullWidth = int.Parse(config["PhotosTransitionSettings:FullWidth"]);
        }

        [AllowAnonymous]
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<PhotoAlbumReturnDto>>> GetGalleryAlbums([FromQuery] int takeFrom = 0, [FromQuery] int takeTo = int.MaxValue)
        {
            var albums = await _photoAlbumsRepository.GetAlbumsPage(takeFrom, takeTo);
            if (albums == null)
                return NoContent();

            var returnAlbums = _mapper.Map<ICollection<PhotoAlbumReturnDto>>(albums);
            foreach(var album in returnAlbums)
            {
                GenerateThumnailsForAlbumPhotos(album);
            }

            return Ok(returnAlbums);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<PhotoAlbumReturnDto>>> GetGalleryAlbum(int id)
        {
            var album = await _photoAlbumsRepository.GetByIdAsync(id);
            if (album == null)
                return NoContent();

            var returnAlbum = _mapper.Map<PhotoAlbumReturnDto>(album);
            return Ok(returnAlbum);
        }

        [HttpPost("addAlbum")]
        public async Task<ActionResult<PhotoAlbumReturnDto>> AddGalleryAlbum(PhotoAlbumAddDto photoAlbumDto) 
        {
            var album = await this._photoAlbumsService.AddAlbum(photoAlbumDto);

            var ret = _mapper.Map<PhotoAlbumReturnDto>(album);
            return Ok(ret);
        }

        [HttpPost("updateAlbum")]
        public async Task<ActionResult> UpdateGalleryAlbum(PhotoAlbumUpdateDto photoAlbumUpdateDto)
        {
            if (!await this._photoAlbumsRepository.AlbumExists(photoAlbumUpdateDto.Id))
                return NotFound($"album o id = {photoAlbumUpdateDto.Id} nie istnieje.");

            await this._photoAlbumsService.EditAlbum(photoAlbumUpdateDto);
            return Ok("Pomyslnie zaktualizowano album");
        }

        [HttpPost("addPhotoToAlbum")]
        public async Task<ActionResult<PhotoFromAlbumDto>> AddPhotoToAlbum([FromForm] AddPhotoToAlbumDto newPhoto)
        {
            if (newPhoto.File == null)
                return BadRequest("W zapytaniu nie przesłano zdjęcia.");

            var album = await _photoAlbumsRepository.GetByIdAsync(newPhoto.AlbumId);

            if (album == null)
                return BadRequest("Nie znaleziono albumu o Id = " + newPhoto.AlbumId);

            var res = await _cloudinaryPhotoService.AddPhotoAsync(newPhoto.File, _fullWidth);
            
            if (res.Error != null)
                return BadRequest("Nie udało się wgrać pliku do magazynu w chmurze." + Environment.NewLine + res.Error.Message);

            var photo = new Photo()
            {
                Comment = newPhoto.Comment,
                PhotoAlbum = album,
                PhotoAlbumId = newPhoto.AlbumId,
                Path = res.SecureUrl.ToString(),
                ThumbnailPath = _cloudinaryPhotoService.GenerateThumbnailPath(res.SecureUrl.ToString(), _thumbnailWidth),
                PublicId = res.PublicId,
                PhotoTarget = "Gallery"
            };

            photo.Id = _photosRepository.Insert(photo);

            return CreatedAtRoute("GetPhoto", new { id = photo.Id }, _mapper.Map<PhotoFromAlbumDto>(photo));
        }

        [HttpDelete("deleteAlbum/{id}")]
        public async Task<ActionResult> DeleteGalleryAlbum(int id)
        {
            if (!await this._photoAlbumsRepository.AlbumExists(id))
                return NotFound($"album o id = {id} nie istnieje.");

            var album = await _photoAlbumsRepository.GetByIdAsync(id);
            foreach(var photo in album.Photos)
            {
                await DeletePhoto(photo.Id);
            }

            this._photoAlbumsRepository.Delete(id);
            return Ok("Pomyslnie usunięto album");
        }
        
        [HttpDelete("delete-photo/{id}")]
        public async Task<ActionResult> DeletePhoto(int id)
        {
            var result = await _photoService.DeletePhoto(id);
            switch (result.Key)
            {
                case 200:
                    return Ok(result.Value);
                case 404:
                    return NotFound(result.Value);
                case 400:
                    return BadRequest(result.Value);
                default:
                    return BadRequest(result.Value);
            }
        }

        [HttpPost("addPhotoWithTarget")]
        public async Task<ActionResult<PhotoWithTargetDto>> AddPhotoWithTarget([FromForm] AddPhotoWithTargetDto newPhoto)
        {
            if (newPhoto.File == null)
                return BadRequest("W zapytaniu nie przesłano zdjęcia.");

            if (newPhoto.TargetWidth == 0)
                newPhoto.TargetWidth = _fullWidth;
            var res = await _cloudinaryPhotoService.AddPhotoAsync(newPhoto.File, newPhoto.TargetWidth);

            if (res.Error != null)
                return BadRequest("Nie udało się wgrać pliku do magazynu w chmurze." + Environment.NewLine + res.Error.Message);

            var photo = new Photo()
            {
                Comment = newPhoto.Comment,
                Path = res.SecureUrl.ToString(),
                PublicId = res.PublicId,
                PhotoTarget = newPhoto.Target
            };

            photo.Id = _photosRepository.Insert(photo);

            return CreatedAtRoute("GetPhoto", new { id = photo.Id }, _mapper.Map<PhotoWithTargetDto>(photo));
        }

        [AllowAnonymous]
        [HttpGet("photosWithTarget_ExcludeGalleryAndWorkers")]
        public async Task<ActionResult<IEnumerable<Photo>>> GetAllPhotosWithTarget_ExcludeGalleryAndWorkers()
        {
            var photos = await _photosRepository.GetAllAsync();
            if (photos == null)
                return NoContent();

            foreach (var photo in photos)
            {
                if (string.IsNullOrEmpty(photo.ThumbnailPath))
                    photo.ThumbnailPath = _cloudinaryPhotoService.GenerateThumbnailPath(photo.Path, _thumbnailWidth);
            }
            var excluded = photos.Where(p => p.PhotoTarget != "Gallery" && p.PhotoTarget != "Workers");

            return Ok(_mapper.Map<IEnumerable<PhotoWithTargetDto>>(excluded));
        }

        [HttpGet("photos")]
        public async Task<ActionResult<IEnumerable<Photo>>> GetAllPhotos()
        {
            var photos = await _photosRepository.GetAllAsync();
            if (photos == null)
                return NoContent();

            foreach(var photo in photos)
            {
                if(string.IsNullOrEmpty(photo.ThumbnailPath))
                    photo.ThumbnailPath = _cloudinaryPhotoService.GenerateThumbnailPath(photo.Path, _thumbnailWidth);
            }
            return Ok(photos);
        }

        [HttpGet("photos/{Id}", Name = "GetPhoto")]
        public async Task<ActionResult<IEnumerable<Photo>>> GetPhotoById(int id)
        {
            var photo = await _photosRepository.GetByIdAsync(id);
            if (photo == null)
                return NoContent();

            return Ok(photo);
        }

        private void GenerateThumnailsForAlbumPhotos(PhotoAlbumReturnDto album)
        {
            foreach(var photo in album.Photos)
            {
                if(photo.ThumbnailPath == null || photo.ThumbnailPath == string.Empty)
                    photo.ThumbnailPath = _cloudinaryPhotoService.GenerateThumbnailPath(photo.Path, _thumbnailWidth);
            }
        }
    }
}
