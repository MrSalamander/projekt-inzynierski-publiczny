﻿using BackendServer.Core.Entities;
using BackendServer.Infrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.API.Controllers
{
    [Authorize]
    public class HtmlInputsController : BaseApiController
    {
        private ISimpleEntityRepository<HtmlInput> _simpleEntityRepository;

        public HtmlInputsController(ISimpleEntityRepository<HtmlInput> simpleEntityRepository)
        {
            _simpleEntityRepository = simpleEntityRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<HtmlInput>>> GetInputs()
        {
            var entities = await this._simpleEntityRepository.GetAllAsync();
            return Ok(entities);
        }

        [HttpPost("add")]
        public async Task<ActionResult<int>> AddInput(HtmlInput input) => Ok(_simpleEntityRepository.Insert(input));

        [HttpPost("update")]
        public async Task<ActionResult<string>> UpdateInput(HtmlInput input)
        {
            _simpleEntityRepository.Update(input);
            return Ok("Pomyślnie zaktualizowano encję");
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult<string>> DeleteInput(int id)
        {
            _simpleEntityRepository.Delete(id);
            return Ok("Pomyślnie usunięto encję");
        }

    }
}
