﻿using BackendServer.Core.Entities;
using BackendServer.Core.Interfaces;
using BackendServer.Infrastructure.Repositories;
using BackendServer.Infrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.API.Controllers
{
    [Authorize]
    public class OffersController : BaseApiController
    {
        private readonly ISimpleEntityRepository<HtmlInput> _simpleHtmlInputsRepository;
        private readonly ISimpleEntityRepository<Photo> _simplePhotosRepository;
        private readonly OffersRepository _simpleOffersRepository;
        private readonly IPhotoService _photoService;

        public OffersController(
            ISimpleEntityRepository<HtmlInput> simpleHtmlInputsRepository, 
            ISimpleEntityRepository<Photo> simplePhotosRepository,
            OffersRepository simpleOffersRepository, 
            IPhotoService photoService)
        {
            _simpleHtmlInputsRepository = simpleHtmlInputsRepository;
            _simplePhotosRepository = simplePhotosRepository;
            _simpleOffersRepository = simpleOffersRepository;
            _photoService = photoService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<Offer[]>> GetOffers()
        {
            var offers = await _simpleOffersRepository.GetAllAsync();
            return Ok(offers);
        }

        [HttpPost("add")]
        public async Task<ActionResult<int>> AddOffer(Offer offer)
        {
            if (offer.HtmlInput != null && offer.HtmlInput.Id != null)
                offer.HtmlInput = await _simpleHtmlInputsRepository.GetByIdAsync(offer.HtmlInput.Id);

            if (offer.Image != null && offer.Image.Id != null)
                offer.Image = await _simplePhotosRepository.GetByIdAsync(offer.Image.Id);

            int newId = _simpleOffersRepository.Insert(offer);
            return Ok(newId);
        }

        [HttpPost("update")]
        public async Task<ActionResult<string>> UpdateOffer(Offer offer)
        {
            var entity = await _simpleOffersRepository.GetByIdAsync(offer.Id);
            
            if(offer.Image != null)
            {
                if (entity.ImageId.HasValue && entity.ImageId.Value != offer.Image.Id)
                {
                    await _photoService.DeletePhoto(entity.ImageId.Value);
                }
                entity.Image = await _simplePhotosRepository.GetByIdAsync(offer.Image.Id);
            } else if(entity.ImageId != null)
            {
                await _photoService.DeletePhoto(entity.ImageId.Value);
            }

            entity.OrderNumber = offer.OrderNumber;
            entity.Title = offer.Title;


            _simpleOffersRepository.Update(entity);
            
            return Ok("Pomyślnie zaktualizowano ofertę.");
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult<string>> DeleteOffer(int id)
        {
            var entity = await _simpleOffersRepository.GetByIdAsync(id);
            if (entity.HtmlInputId.HasValue)
                _simpleHtmlInputsRepository.Delete(entity.HtmlInputId.Value);

            if (entity.ImageId.HasValue)
                await _photoService.DeletePhoto(entity.ImageId.Value);

            _simpleOffersRepository.Delete(entity.Id);

            return Ok("Pomyślnie usunięto ofertę.");
        }
    }
}
