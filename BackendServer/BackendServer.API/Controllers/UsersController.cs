﻿using BackendServer.API.Entities;
using BackendServer.Core.DTOs.User;
using BackendServer.Infrastructure.Repositories;
using BackendServer.Core.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BackendServer.API.Controllers
{
    //[Authorize]
    public class UsersController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IUserRepository _userRepository;

        public UsersController(IUserService userService, IUserRepository userRepository)
        {
            this._userService = userService;
            this._userRepository = userRepository;
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<ActionResult<LoggedUserDto>> Login(LoginUserDto loginDto)
        {

            if(!await _userService.UserExists(loginDto.Username))
                return Unauthorized("Niepoprawna nazwa użytkownika.");

            if (!await _userService.PasswordMatches(loginDto))
                return Unauthorized("Niepoprawne hasło.");

            LoggedUserDto user = _userService.CreateUserDtoWithToken(loginDto);
            return Ok(user);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<SimpleUserDto>>> GetUsers()
        {
            var users = await _userRepository.GetSimpleUsersAsync();
            return Ok(users);
        }

        [HttpGet("{Username}")]
        public async Task<ActionResult<SimpleUserDto>> GetUser(string username)
        {
            var user = await _userRepository.GetSimpleUserAsync(username);
            if (user == null)
                return NotFound($"Nie odnaleziono użytkownika o nazwie {username}.");
            else
                return Ok(user);
        }

        [HttpPost("update-my-password")]
        public async Task<ActionResult> UpdateCurrentUserPassword(ChangeCurrentUserPasswordDto changeCurrentUserPasswordDto)
        {
            var username = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (!await _userService.PasswordMatches(new LoginUserDto(username, changeCurrentUserPasswordDto.CurrentPassword)))
                return Unauthorized("Niepoprawne hasło.");

            _userService.UpdateUsersPassword(username, changeCurrentUserPasswordDto.NewPassword);
            return Ok("Pomyślnie zmieniono hasło.");
        }

        [HttpPost("Register")]
        //[Authorize(Roles = PermissionConstRoles.Admin)]
        public async Task<ActionResult> Register(RegisterUserDto user)
        {
            if (await _userService.UserExists(user.Username))
            {
                return BadRequest("Nazwa użytkownika jest zajęta.");
            }

            if (!PermissionNameMatchesConst(user.PermissionName))
                return BadRequest("Nie znaleziono uprawnienia - błędna nazwa");

            _userService.RegisterUser(user);
            return Ok("Pomyślnie dodano użytkownika.");
        }

        [HttpDelete("delete/{username}")]
        //[Authorize(Roles = PermissionConstRoles.Admin)]
        public async Task<ActionResult> DeleteUser(string username)
        {
            var userExists = await _userService.UserExists(username);
            if (!userExists)
                return NotFound($"Nie odnaleziono użytkownika o nazwie {username}.");

            var user = await _userRepository.GetUserAsync(username);

            if (username.ToLower() == "admin" || username.ToLower() == "administrator")
                return BadRequest("Konta administratora nie można usunąć.");

            _userRepository.Delete(user);

            return Ok("Pomyślnie usunięto użytkownika.");
        }

        [HttpPost("update-permission")]
        //[Authorize(Roles = PermissionConstRoles.Admin)]
        public async Task<ActionResult> UpdateUserPermission(SimpleUserDto simpleUserDto)
        {
            var userExists = await _userService.UserExists(simpleUserDto.Username);
            if (!userExists)
                return NotFound($"Nie odnaleziono użytkownika o nazwie {simpleUserDto.Username}.");

            if (!PermissionNameMatchesConst(simpleUserDto.PermissionLevelName))
                return NotFound("Nie znaleziono uprawnienia - błędna nazwa");

            await _userService.UpdateUsersPermission(simpleUserDto);
            return Ok("Pomyślnie zmieniono uprawnienie.");
        }

        #region private methods
        private bool PermissionNameMatchesConst(string permissionLevelName)
        {
            if (PermissionConstRoles.Admin == permissionLevelName) return true;
            if (PermissionConstRoles.MlodszyEdytor == permissionLevelName) return true;
            if (PermissionConstRoles.StarszyEdytor == permissionLevelName) return true;
            return false;
        }
        #endregion
    }
}
