﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.API.Entities
{
    public static class PermissionConstRoles
    {
        public const string Admin = "Admin";
        public const string StarszyEdytor = "Starszy edytor";
        public const string MlodszyEdytor = "Młodszy edytor";
    }
}
