﻿using BackendServer.Infrastructure.Data;
using BackendServer.Infrastructure.Mappers;
using BackendServer.Infrastructure.Repositories;
using BackendServer.Infrastructure.Services;
using BackendServer.Core.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using BackendServer.Infrastructure.Repositories.Interfaces;
using BackendServer.Infrastructure.Repositories.GenericPatterns;
using BackendServer.Infrastructure.ExternalApi.Cloudinary;
using BackendServer.Core.Interfaces;
using BackendServer.Core.Entities;

namespace BackendServer.API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddMvc(option => option.EnableEndpointRouting = false)
                .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IUserService, UserService>();

            services.Configure<CloudinarySettings>(config.GetSection("CloudinarySettings"));
            services.AddScoped<ICloudinaryPhotoService, CloudinaryPhotoService>();

            services.AddScoped(typeof(ISimpleEntityRepository<>), typeof(SimpleEntityRepository<>));
            services.AddScoped<IWorkersRepository, WorkersRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            
            services.AddScoped<IPhotoAlbumsRepository, PhotoAlbumsRepository>();
            services.AddScoped<IPhotoAlbumsService, PhotoAlbumsService>();
            services.AddScoped<IPhotoService, PhotoService>();

            

            services.AddScoped(typeof(OffersRepository));


            services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);


            services.AddDbContext<DataContext>(options =>
            {
                //options.UseSqlite(config.GetConnectionString("SqLite_Connection"));
                options.UseSqlite(config.GetConnectionString("SqLiteRelease_Connection"));
            });

            return services;
        }
    }
}
