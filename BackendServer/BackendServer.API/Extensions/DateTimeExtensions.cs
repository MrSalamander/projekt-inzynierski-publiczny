﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.API.Extensions
{
    public static class DateTimeExtensions
    {
        public static int CalculateAge(this DateTime dateTimeExtended)
        {
            var today = DateTime.Today;
            var age = today.Year - dateTimeExtended.Year;
            var aimedPreviousDateForCheck = today.AddYears(-age);
            if (dateTimeExtended.Date > aimedPreviousDateForCheck)
            {
                age--;
            }
            return age;
        }
    }
}
