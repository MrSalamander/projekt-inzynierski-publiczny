﻿using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.ExternalApi.Cloudinary
{
    public interface ICloudinaryPhotoService
    {
        Task<ImageUploadResult> AddPhotoAsync(IFormFile file, int fullWidth);
        Task<ImageUploadResult> AddPhotoAsync(Stream imgStream, string fileName, int fullWidth);
        Task<ImageUploadResult> AddPhotoAsync(Stream imgStream, string fileName, int sizeInPixels, bool useHeightInsteadOfWidth = false);
        Task<DeletionResult> DeletePhotoAsync(string publicId);

        string GenerateThumbnailPath(string path, int width);
    }
}
