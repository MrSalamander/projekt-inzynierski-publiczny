﻿using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CloudinaryDotNet;
using Microsoft.Extensions.Options;
using System.IO;

namespace BackendServer.Infrastructure.ExternalApi.Cloudinary
{
    public class CloudinaryPhotoService : ICloudinaryPhotoService
    {
        private readonly CloudinaryDotNet.Cloudinary _cloudinary;
        public CloudinaryPhotoService(IOptions<CloudinarySettings> config)
        {
            var acc = new Account(config.Value.CloudName, config.Value.ApiKey, config.Value.ApiSecret);
            _cloudinary = new CloudinaryDotNet.Cloudinary(acc);
        }

        public async Task<ImageUploadResult> AddPhotoAsync(IFormFile file, int fullWidth)
        {
            await using var stream = file.OpenReadStream();
            return await AddPhotoAsync(stream, file.FileName, fullWidth);
        }

        public async Task<ImageUploadResult> AddPhotoAsync(Stream imgStream, string fileName, int sizeInPixels, bool profilePic = false)
        {
            var uploadResult = new ImageUploadResult();
            if (imgStream.Length > 0)
            {
                Transformation transformation;
                if (!profilePic)
                    transformation = new Transformation().Width(sizeInPixels).Crop("scale");
                else
                {
                    sizeInPixels = (int)((double)sizeInPixels * 1.5);
                    transformation = new Transformation()
                        .Gravity("auto").Height(sizeInPixels).Width(sizeInPixels).Crop("fill").Chain();
                }

                var uploadParams = new ImageUploadParams
                {
                    File = new FileDescription(fileName, imgStream),
                    Transformation = transformation
                };

                uploadResult = await _cloudinary.UploadAsync(uploadParams);
            }
            return uploadResult;
        }

        public async Task<ImageUploadResult> AddPhotoAsync(Stream imgStream, string fileName, int fullWidth) => await AddPhotoAsync(imgStream, fileName, fullWidth, false);

        public async Task<DeletionResult> DeletePhotoAsync(string publicId)
        {
            var deleteParams = new DeletionParams(publicId);
            return await _cloudinary.DestroyAsync(deleteParams);
        }

        StringBuilder sb = new StringBuilder();
        public string GenerateThumbnailPath(string path, int width)
        {
            sb.Clear();
            var splitter = "image/upload/";
            var splitted = path.Split("image/upload/");

            if (splitted.Length != 2)
                return string.Empty;

            sb.Append(splitted[0]);
            sb.Append(splitter);
            sb.Append("c_scale,w_");
            sb.Append(width);
            sb.Append("/");
            sb.Append(splitted[1]);


            var thumbnail = sb.ToString();
            return thumbnail;
        }
    }
}
