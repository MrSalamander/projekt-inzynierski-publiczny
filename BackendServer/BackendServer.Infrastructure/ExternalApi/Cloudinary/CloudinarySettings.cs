﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendServer.Infrastructure.ExternalApi.Cloudinary
{
    public class CloudinarySettings
    {
        public string CloudName { get; set; }
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
    }
}
