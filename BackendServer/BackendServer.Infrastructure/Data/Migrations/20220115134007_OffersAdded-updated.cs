﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendServer.Infrastructure.Data.Migrations
{
    public partial class OffersAddedupdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_ImageId",
                table: "Offers");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers",
                column: "HtmlInputId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offers_ImageId",
                table: "Offers",
                column: "ImageId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_ImageId",
                table: "Offers");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers",
                column: "HtmlInputId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_ImageId",
                table: "Offers",
                column: "ImageId");
        }
    }
}
