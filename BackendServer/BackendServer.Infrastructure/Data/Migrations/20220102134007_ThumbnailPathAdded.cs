﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendServer.Infrastructure.Data.Migrations
{
    public partial class ThumbnailPathAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ThumbnailPath",
                table: "Photos",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ThumbnailPath",
                table: "Photos");
        }
    }
}
