﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendServer.Infrastructure.Data.Migrations
{
    public partial class OffersAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Photos_PhotoId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_PhotoId",
                table: "Offers");

            migrationBuilder.RenameColumn(
                name: "PhotoId",
                table: "Offers",
                newName: "ImageId");

            migrationBuilder.AddColumn<int>(
                name: "OrderNumber",
                table: "Offers",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Offers",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EmploymentOffers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    HtmlInputId = table.Column<int>(type: "INTEGER", nullable: true),
                    PhotoId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmploymentOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmploymentOffers_HtmlInputs_HtmlInputId",
                        column: x => x.HtmlInputId,
                        principalTable: "HtmlInputs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmploymentOffers_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers",
                column: "HtmlInputId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_ImageId",
                table: "Offers",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_EmploymentOffers_HtmlInputId",
                table: "EmploymentOffers",
                column: "HtmlInputId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmploymentOffers_PhotoId",
                table: "EmploymentOffers",
                column: "PhotoId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Photos_ImageId",
                table: "Offers",
                column: "ImageId",
                principalTable: "Photos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Photos_ImageId",
                table: "Offers");

            migrationBuilder.DropTable(
                name: "EmploymentOffers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_ImageId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OrderNumber",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Offers");

            migrationBuilder.RenameColumn(
                name: "ImageId",
                table: "Offers",
                newName: "PhotoId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers",
                column: "HtmlInputId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offers_PhotoId",
                table: "Offers",
                column: "PhotoId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Photos_PhotoId",
                table: "Offers",
                column: "PhotoId",
                principalTable: "Photos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
