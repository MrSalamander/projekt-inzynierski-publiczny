﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendServer.Infrastructure.Data.Migrations
{
    public partial class OfferaddedHtmlInput2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_HtmlInputs_Target",
                table: "HtmlInputs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_HtmlInputs_Target",
                table: "HtmlInputs",
                column: "Target",
                unique: true);
        }
    }
}
