﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendServer.Infrastructure.Data.Migrations
{
    public partial class OfferaddedHtmlInput : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HtmlInputText",
                table: "Offers");

            migrationBuilder.AddColumn<int>(
                name: "HtmlInputId",
                table: "Offers",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers",
                column: "HtmlInputId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_HtmlInputs_HtmlInputId",
                table: "Offers",
                column: "HtmlInputId",
                principalTable: "HtmlInputs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offers_HtmlInputs_HtmlInputId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_HtmlInputId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "HtmlInputId",
                table: "Offers");

            migrationBuilder.AddColumn<string>(
                name: "HtmlInputText",
                table: "Offers",
                type: "TEXT",
                nullable: true);
        }
    }
}
