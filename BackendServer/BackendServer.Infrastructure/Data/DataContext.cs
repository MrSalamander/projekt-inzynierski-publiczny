﻿using BackendServer.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendServer.Infrastructure.Data
{
    public class DataContext : DbContext
    {
        //public readonly ILoggerFactory MyLoggerFactory;
        public DataContext(DbContextOptions options) : base(options){ /*MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); }); */ }

        public DbSet<User> Users { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<PhotoAlbum> PhotoAlbums { get; set; }
        public DbSet<Worker> Workers { get; set; }

        public DbSet<ContactParam> ContactParams { get; set; }
        public DbSet<EmploymentOffer> EmploymentOffers { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<HtmlInput> HtmlInputs{ get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    base.OnConfiguring(optionsBuilder);
        //    optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        //}
    }
}
