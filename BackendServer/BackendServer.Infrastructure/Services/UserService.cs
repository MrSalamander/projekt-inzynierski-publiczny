﻿using BackendServer.Core.Entities;
using BackendServer.Core.DTOs.User;
using BackendServer.Infrastructure.Repositories;
using BackendServer.Infrastructure.Repositories.Interfaces;
using BackendServer.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ITokenService _tokenService;
        private ISimpleEntityRepository<Permission> _permissionRepository;

        #region Ctor
        public UserService(IUserRepository userRepository, ITokenService tokenService, ISimpleEntityRepository<Permission> permissionRepository)
        {
            this._userRepository = userRepository;
            this._tokenService = tokenService;
            _permissionRepository = permissionRepository;
        }
        #endregion

        #region Login methods
        public async Task<bool> UserExists(string userName) => await _userRepository.GetUserAsync(userName) != null;
        
        public async Task<bool> PasswordMatches(LoginUserDto loginDto)
        {
            var user = await _userRepository.GetUserAsync(loginDto.Username);
            var salt = user.PasswordSalt;
            using var hmac = new HMACSHA512(salt);

            var computeHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));

            for (int i = 0; i < computeHash.Length; i++)
            {
                if (computeHash[i] != user.PasswordHash[i])
                    return false;
            }
            return true;
        }

        public LoggedUserDto CreateUserDtoWithToken(LoginUserDto loginDto)
        {
            if(!this.UserExists(loginDto.Username).Result)
                throw new ArgumentException("There is no user with this username");

            var user = _userRepository.GetUserAsync(loginDto.Username).Result;
            return new LoggedUserDto
            {
                Username = loginDto.Username,
                PermissionLevelName = user.PermissionLevel.PermissionName,
                Token = _tokenService.CreateToken(user)
            };
        }
        #endregion

        public void RegisterUser(RegisterUserDto newUser)
        {
            var permissions = _permissionRepository.GetAllAsync().Result;
            var permission = permissions.SingleOrDefault(x => x.PermissionName == newUser.PermissionName);
            if (permission == null)
                throw new ArgumentException($"Permission '{newUser.PermissionName}' not found");

            using var hmac = new HMACSHA512();
            var user = new User()
            {
                Username = newUser.Username,
                PermissionLevel = permission,
                PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(newUser.Password)),
                PasswordSalt = hmac.Key
            };

            _userRepository.Add(user);
        }

        public void UpdateUsersPassword(string username, string newPassword)
        {
            var userEntity = _userRepository.GetUserAsync(username).Result;
            using var hmac = new HMACSHA512();
            userEntity.PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(newPassword));
            userEntity.PasswordSalt = hmac.Key;
            _userRepository.Update(userEntity);
        }

        public async Task UpdateUsersPermission(SimpleUserDto simpleUserDto)
        {
            var userEntity  = await _userRepository.GetUserAsync(simpleUserDto.Username);
            var permissions = await _permissionRepository.GetAllAsync();

            var permission = permissions.SingleOrDefault(x => x.PermissionName == simpleUserDto.PermissionLevelName);
            
            if (permission == null)
                throw new ArgumentNullException($"Nie znaleziono uprawnienia {simpleUserDto.PermissionLevelName}");
            
            userEntity.PermissionLevel = permission;
            _userRepository.Update(userEntity);
        }
    }
}
