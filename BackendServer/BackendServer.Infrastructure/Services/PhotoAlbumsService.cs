﻿using AutoMapper;
using BackendServer.Core.Entities;
using BackendServer.Core.DTOs.Albums;
using BackendServer.Infrastructure.ExternalApi.Cloudinary;
using BackendServer.Infrastructure.Repositories.Interfaces;
using BackendServer.Core.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Services
{
    public class PhotoAlbumsService : IPhotoAlbumsService
    {
        private readonly IPhotoAlbumsRepository _photoAlbumsRepository;
        private readonly ICloudinaryPhotoService _cloudinaryPhotoService;
        private readonly ISimpleEntityRepository<Photo> _photoRepository;
        private readonly IMapper _mapper;

        public PhotoAlbumsService(ISimpleEntityRepository<Photo> photoRepository, IPhotoAlbumsRepository photoAlbumsRepository, ICloudinaryPhotoService cloudinaryPhotoService,IMapper mapper)
        {
            _photoRepository = photoRepository;
            _photoAlbumsRepository = photoAlbumsRepository;
            _cloudinaryPhotoService = cloudinaryPhotoService;
            _mapper = mapper;
        }

        #region Gallery
        public async Task<PhotoAlbum> AddAlbum(PhotoAlbumAddDto photoAlbumDto)
        {
            var photoAlbum = _mapper.Map<PhotoAlbum>(photoAlbumDto);

            var id = _photoAlbumsRepository.Insert(photoAlbum);
            var album = await _photoAlbumsRepository.GetByIdAsync(id);
            return album;
        }


        public async Task<PhotoAlbum> EditAlbum(PhotoAlbumUpdateDto photoAlbumUpdateDto)
        {
            var photoAlbums = await _photoAlbumsRepository.GetAllAsync();
            var photoAlbum = photoAlbums.SingleOrDefault(x => x.Id == photoAlbumUpdateDto.Id);
            if (photoAlbum == null)
                throw new ArgumentException("Nie znaleziono albumu o id = " + photoAlbumUpdateDto.Id);

            photoAlbum.Name = photoAlbumUpdateDto.Name;
            photoAlbum.Comment = photoAlbumUpdateDto.Comment;
            
            _photoAlbumsRepository.Update(photoAlbum);
            return photoAlbum;
        }
        #endregion


    }
}
