﻿using BackendServer.Core.Entities;
using BackendServer.Core.Interfaces;
using BackendServer.Infrastructure.ExternalApi.Cloudinary;
using BackendServer.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly ICloudinaryPhotoService _cloudinaryPhotoService;
        private readonly ISimpleEntityRepository<Photo> _simplePhotosRepository;

        public PhotoService(ICloudinaryPhotoService cloudinaryPhotoService, ISimpleEntityRepository<Photo> simplePhotosRepository)
        {
            _cloudinaryPhotoService = cloudinaryPhotoService;
            _simplePhotosRepository = simplePhotosRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Key ==> Html Code ex. 200 (ok) <br/> 
        ///          value ==> message (PL) </returns>
        public async Task<KeyValuePair<int, string>> DeletePhoto(int id)
        {
            var photo = await _simplePhotosRepository.GetByIdAsync(id);

            if (photo == null)
                return new KeyValuePair<int, string>(404, $"Nie znaleziono zdjęcia o Id = {id}");

            if (photo.PublicId != null)
            {
                var result = await _cloudinaryPhotoService.DeletePhotoAsync(photo.PublicId);
                if (result.Error != null)
                    return new KeyValuePair<int, string>(400, $"Nie udało się usunąć pliku z magazynu w chmurze.{Environment.NewLine}{result.Error.Message}");
            }

            photo.Worker = null;
            photo.PhotoAlbum = null;
            photo.Offer = null;

            _simplePhotosRepository.Delete(id);

            return new KeyValuePair<int, string>(200, "Pomyślnie usunięto zdjęcie");
        }
    }
}
