﻿using BackendServer.Core.Entities;
using BackendServer.Infrastructure.Data;
using BackendServer.Infrastructure.Repositories.GenericPatterns;
using BackendServer.Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Repositories
{
    public class WorkersRepository : SimpleEntityRepository<Worker>, IWorkersRepository
    {
        private readonly ISimpleEntityRepository<Photo> _photoRepository;

        public WorkersRepository(DataContext context, ISimpleEntityRepository<Photo> photoRepository) : base(context)
        {
            this._photoRepository = photoRepository;
        }

        public override async Task<IEnumerable<Worker>> GetAllAsync() => await _entities.Include(x => x.ProfilePhoto).ToListAsync();

        public override async Task<Worker> GetByIdAsync(int id) => await _entities.Include(x => x.ProfilePhoto).SingleOrDefaultAsync(s => s.Id == id);

        public override void Delete(int id)
        {
            var entity = GetByIdAsync(id).Result;
            if (entity != null && entity.ProfilePhoto != null)
                _photoRepository.Delete(entity.ProfilePhoto.Id);
            base.Delete(id);
        }
    }
}
