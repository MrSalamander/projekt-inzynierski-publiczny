﻿using BackendServer.Core.Entities;
using BackendServer.Infrastructure.Data;
using BackendServer.Core.DTOs.Photos;
using BackendServer.Infrastructure.Repositories.GenericPatterns;
using BackendServer.Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Repositories
{
    public class PhotoAlbumsRepository : SimpleEntityRepository<PhotoAlbum>, IPhotoAlbumsRepository
    {
        private readonly ISimpleEntityRepository<Photo> _photoRepository;

        public PhotoAlbumsRepository(DataContext context, ISimpleEntityRepository<Photo> photoRepository) : base(context)
        {
            this._photoRepository = photoRepository;
        }

        public async Task<bool> AlbumExists(int id) => await this.GetByIdAsync(id) != null;
        
        public override async Task<IEnumerable<PhotoAlbum>> GetAllAsync() => await _entities.Include(x => x.Photos).ToListAsync();

        public override async Task<PhotoAlbum> GetByIdAsync(int id) => await _entities.Include(x => x.Photos).SingleOrDefaultAsync(s => s.Id == id);

        public override void Delete(int id)
        {
            var entity = GetByIdAsync(id).Result;
            if (entity != null && entity.Photos.Count > 0)
                foreach(var photo in entity.Photos)
                {
                    _photoRepository.Delete(photo.Id);
                }
            base.Delete(id);
        }

        public async Task<IEnumerable<PhotoAlbum>> GetAlbumsPage(int from, int to)
        {
            var photoAlbums = this._entities.Include(x => x.Photos).ToList();
            List<PhotoAlbum> list = new List<PhotoAlbum>();

            for(int i = 0; i < photoAlbums.Count; i++)
            {
                if (i >= from && i <= to)
                    list.Add(photoAlbums[i]);
            }
            return list;
        }


    }
}
