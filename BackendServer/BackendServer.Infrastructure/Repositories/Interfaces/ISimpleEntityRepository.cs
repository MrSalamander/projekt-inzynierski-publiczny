﻿using BackendServer.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Repositories.Interfaces
{
    public interface ISimpleEntityRepository<T> where T : DatabaseObject
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        int Insert(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}
