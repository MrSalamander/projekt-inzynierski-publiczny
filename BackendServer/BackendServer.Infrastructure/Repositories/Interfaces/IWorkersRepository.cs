﻿using BackendServer.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendServer.Infrastructure.Repositories.Interfaces
{
    public interface IWorkersRepository : ISimpleEntityRepository<Worker>
    {
    }
}
