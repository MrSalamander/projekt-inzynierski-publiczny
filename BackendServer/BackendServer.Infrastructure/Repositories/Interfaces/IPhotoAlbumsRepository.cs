﻿using BackendServer.Core.Entities;
using BackendServer.Core.DTOs.Photos;
using BackendServer.Infrastructure.Repositories.GenericPatterns;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Repositories.Interfaces
{
    public interface IPhotoAlbumsRepository : ISimpleEntityRepository<PhotoAlbum>
    {
        Task<IEnumerable<PhotoAlbum>> GetAlbumsPage(int startId, int endId);
        Task<bool> AlbumExists(int id);
    }
}
