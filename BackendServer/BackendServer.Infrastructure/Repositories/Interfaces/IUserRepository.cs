﻿using BackendServer.Core.Entities;
using BackendServer.Core.DTOs.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Repositories
{
    public interface IUserRepository
    {
        void Update(User user);
        void Delete(User user);
        void Add(User user);
        
        Task<User> GetUserAsync(string username);
        Task<IEnumerable<User>> GetUsersAsync();

        Task<SimpleUserDto> GetSimpleUserAsync(string username);
        Task<IEnumerable<SimpleUserDto>> GetSimpleUsersAsync();


        Task<bool> SaveAllAsync();
    }
}
