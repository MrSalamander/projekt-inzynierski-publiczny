﻿using BackendServer.Core.Entities;
using BackendServer.Infrastructure.Data;
using BackendServer.Infrastructure.Repositories.GenericPatterns;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Repositories
{
    public class OffersRepository: SimpleEntityRepository<Offer>
    {
        public OffersRepository(DataContext dataContext) : base(dataContext)
        {
            this._context.Offers
                .Include(offer => offer.HtmlInput)
                .Include(offer => offer.Image);

        }

        public async override Task<IEnumerable<Offer>> GetAllAsync()
        {
            return await this._context.Offers
                .Include(offer => offer.HtmlInput)
                .Include(offer => offer.Image).ToListAsync();
        }
    }
}
