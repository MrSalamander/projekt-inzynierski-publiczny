﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BackendServer.Core.Entities;
using BackendServer.Infrastructure.Data;
using BackendServer.Core.DTOs.User;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UserRepository(DataContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public void Add(User user)
        {
            _context.Add(user);
            _context.SaveChanges();
        }

        public void Delete(User user)
        {
            _context.Remove(user);
            _context.SaveChanges();
        }
        
        public void Update(User user)
        {
            var entry = _context.Entry(user);
            entry.Entity.PermissionLevel = user.PermissionLevel;
            entry.Entity.PasswordHash = user.PasswordHash;
            entry.Entity.PasswordSalt = user.PasswordSalt;
            entry.State = EntityState.Modified;
            _context.SaveChanges();
        }

        public async Task<SimpleUserDto> GetSimpleUserAsync(string username) => await _context.Users
                .Where(x => x.Username == username)
                .ProjectTo<SimpleUserDto>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();

        public async Task<IEnumerable<SimpleUserDto>> GetSimpleUsersAsync() => await _context.Users
            .ProjectTo<SimpleUserDto>(_mapper.ConfigurationProvider)
            .ToListAsync();

        public async Task<User> GetUserAsync(string username) => await _context.Users
            .Include(x => x.PermissionLevel)
            .SingleOrDefaultAsync(x => x.Username == username);

        public async Task<IEnumerable<User>> GetUsersAsync() => await _context.Users
            .Include(x => x.PermissionLevel)
            .ToListAsync();

        public async Task<bool> SaveAllAsync() => await _context.SaveChangesAsync() > 0;

        public async Task<bool> UserExists(string userName) => await _context.Users.AnyAsync(x => x.Username == userName);

    }
}
