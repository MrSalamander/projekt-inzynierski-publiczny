﻿using BackendServer.Core.Entities;
using BackendServer.Infrastructure.Data;
using BackendServer.Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackendServer.Infrastructure.Repositories.GenericPatterns
{
    public class SimpleEntityRepository<EntityType> : ISimpleEntityRepository<EntityType> where EntityType : DatabaseObject
    {
        protected readonly DataContext _context;
        protected DbSet<EntityType> _entities;

        public SimpleEntityRepository(DataContext context)
        {
            this._context = context;
            _entities = _context.Set<EntityType>();
        }

        public virtual async Task<IEnumerable<EntityType>> GetAllAsync() => await _entities.ToListAsync();

        public virtual async Task<EntityType> GetByIdAsync(int id) => await _entities.SingleOrDefaultAsync(s => s.Id == id);

        public virtual int Insert(EntityType entity)
        {
            if (entity == null)
                throw new ArgumentNullException($"Przekazana encja jest pusta");

            var tmp =_entities.Add(entity);
            _context.SaveChanges();
            return tmp.Entity.Id;
        }

        public virtual void Update(EntityType entity)
        {
            if (entity == null)
                throw new ArgumentNullException($"Przekazana encja jest pusta");
            _entities.Update(entity);
            _context.SaveChanges();
        }

        public virtual void Delete(int id)
        {
            EntityType entity = GetByIdAsync(id).Result;
            if (entity == null)
                throw new ArgumentNullException($"Nie znaleziono encji o wskazanym id = {id}");
            _entities.Remove(entity);
            _context.SaveChanges();
        }
    }
}
