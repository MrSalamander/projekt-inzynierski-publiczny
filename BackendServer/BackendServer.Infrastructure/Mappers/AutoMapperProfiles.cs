﻿using AutoMapper;
using BackendServer.Core.Entities;
using BackendServer.Core.DTOs;
using BackendServer.Core.DTOs.Albums;
using BackendServer.Core.DTOs.Photos;
using BackendServer.Core.DTOs.User;
using BackendServer.Core.DTOs.Workers;

namespace BackendServer.Infrastructure.Mappers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            #region Users
            CreateMap<LoginUserDto, User>();
            CreateMap<User, LoggedUserDto>();

            CreateMap<User, SimpleUserDto>()
                .ForMember(
                dest => dest.PermissionLevelName,
                opt => opt.MapFrom(src => src.PermissionLevel.PermissionName));
            CreateMap<LoggedUserDto, User>();
            #endregion

            #region Photos
            CreateMap<PhotoAlbumAddDto, PhotoAlbum>();
            CreateMap<PhotoAlbum, PhotoAlbumReturnDto>();

            CreateMap<Photo, PhotoFromAlbumDto>();
            CreateMap<Photo, PhotoWithTargetDto>();
            CreateMap<Photo, PhotoDto>();
            #endregion

            CreateMap<EmploymentOffer, EmploymentOfferDto>();
            CreateMap<EmploymentOfferDto, EmploymentOffer>();

            #region Workers
            CreateMap<WorkerInputDto, Worker>();
            CreateMap<Worker, WorkerReturnDto>().
                ForMember(dest => dest.ProfilePhoto, opt => opt.MapFrom(src => src.ProfilePhoto));
            CreateMap<WorkerInputDto, Worker>();
            #endregion
        }
    }
}
