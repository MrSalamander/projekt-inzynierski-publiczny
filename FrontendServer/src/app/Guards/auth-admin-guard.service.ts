import { permissionLevelAdmin } from './../StaticParams/permissions';
import { UsersService } from 'src/app/services/users.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthAdminGuard implements CanActivate {

  constructor( private router: Router, private usersService: UsersService  ) { }

  canActivate() {
    if(this.usersService.loggedIn) 
      return true;
      
    this.router.navigate(['/login'])
    return false;
  }
}
