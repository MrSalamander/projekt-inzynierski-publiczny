import {DomSanitizer} from '@angular/platform-browser';
import {PipeTransform, Pipe, SecurityContext} from '@angular/core';

@Pipe({ name: 'unsafeHtmlPipe'})
export class UnsafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    if(value)
      return this.sanitized.bypassSecurityTrustHtml(value);
    else
      return undefined
  }
}