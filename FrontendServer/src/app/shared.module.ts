import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastContainerModule, ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      positionClass: 'toast-top-left'
    }),
    ToastContainerModule
  ],
  exports: [
    ToastrModule
  ]
})
export class SharedModule { }
