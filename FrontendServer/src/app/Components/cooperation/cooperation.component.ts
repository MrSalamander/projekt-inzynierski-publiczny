import { GalleryService } from 'src/app/services/gallery.service';
import { UsersService } from 'src/app/services/users.service';
import { HtmlInputsService } from 'src/app/services/html-inputs.service';
import { Component, OnInit } from '@angular/core';
import { HtmlInput } from 'src/app/Models/htmlInput';
import { PhotoWithTargetDto } from 'src/app/Models/Photos/photo-with-target-dto';

@Component({
  selector: 'app-cooperation',
  templateUrl: './cooperation.component.html',
  styleUrls: ['./cooperation.component.css']
})
export class CooperationComponent implements OnInit {
  public mainHtmlInput: HtmlInput;
  public htmlContentPreview: boolean = false;
  public htmlContentPreviewValue: string;
  public coopPanelTarget = "CooperationPanelTarget"

  public upperImage: PhotoWithTargetDto;
  public reloadCallbackFunctionWithPayload = (photoWithTargetDto) => this.setPhoto(photoWithTargetDto)

  constructor(
    public usersService: UsersService, 
    private galleryService: GalleryService, 
    private htmlInputsService: HtmlInputsService) { }

  ngOnInit(): void {
    this.loadImage();
    this.loadHtmlInput();
  }

  loadHtmlInput() {
    this.htmlInputsService.getHtmlInputByTarget(this.coopPanelTarget).then(resp => {
      this.mainHtmlInput = resp;
    })
  }

  loadImage() {
    this.galleryService.getPhotosWithTargetPromise().then(value => {
      this.upperImage = value.find(x => x.photoTarget == this.coopPanelTarget);
    })
  }

  removePhotoClick(photo: PhotoWithTargetDto){  
    if (confirm("Czy na pewno chcesz usunąć to zdjęcie?")) {
      this.galleryService.deletePhotoWithTarget(photo).subscribe( resp => {
        this.upperImage = null;
      })
    }
  }

  setPhoto(photo: PhotoWithTargetDto){
    this.upperImage = photo;
  }
}
