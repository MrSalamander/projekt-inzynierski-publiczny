import { PhotoWithTargetDto } from './../../Models/Photos/photo-with-target-dto';
import { SingleOfferTabContent } from './../../Models/LocalUsageClasses/singleOfferTabContent';
import { Component, OnInit} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HtmlInputsService } from 'src/app/services/html-inputs.service';
import { GalleryService } from 'src/app/services/gallery.service';
import { OffersService } from 'src/app/services/offers.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {

  public reloadCallbackFunctionWithPayload = (photoWithTargetDto) => this.setPhoto(photoWithTargetDto)
  public acceptButtonCallback = () => this.addOrUpdateNewOffer();
  public removeButtonCallback = () => this.removeOffer(this.selectedTab);

  //#region Properties
  public tabs: SingleOfferTabContent[] = [];
  public selectedTab: SingleOfferTabContent;

  public canAddNewTab = true;
  public addingNewTab = false;

  public offerPanelTarget = "OfferPanelTarget_"
    public newTab: SingleOfferTabContent;
  //#endregion

  //#region ctor
  constructor(
    public usersService: UsersService, 
    private offersService: OffersService,
    private toastrService: ToastrService,
    private htmlInputsService: HtmlInputsService,
    private galleryService: GalleryService
    ) { }

  ngOnInit(): void {
    this.loadTabs();
  }

  loadTabs() {
    this.offersService.getOffers().subscribe(resp => {
      this.tabs = resp;
    })
  }
  //#endregion
  setPhoto(photo: PhotoWithTargetDto){
    if(this.addingNewTab){
      this.newTab.image= photo;
    }else {
      this.selectedTab.image = photo;
      this.offersService.updateOffer(this.selectedTab).subscribe()
    }
  }
  
  removePhotoClick(photo: PhotoWithTargetDto){  
    if (confirm("Czy na pewno chcesz usunąć to zdjęcie?")) {
      if(this.addingNewTab){
        this.galleryService.deletePhotoWithTarget(photo).subscribe( resp => {
        this.newTab.image = null;
        })
      }
      else{
        this.selectedTab.image = null;
        this.offersService.updateOffer(this.selectedTab).subscribe( )
      }
    }
  }
  
  addNewOfferTab() {
    this.newTab = new SingleOfferTabContent();
    this.addingNewTab = true;
    this.canAddNewTab = false;
  }

  addOrUpdateNewOffer(){
    if(this.addingNewTab){
      this.offersService.addOffer(this.newTab).subscribe(resp => {
        this.newTab.id = resp;
        console.log(this.newTab);
        this.tabs.push(this.newTab);
        this.toastrService.success("Pomyślnie dodano nową ofertę.")
        this.clearAndHideNewOfferTab()
      }, error => {
        this.galleryService.deletePhotoWithTarget(this.newTab.image).subscribe();
        this.htmlInputsService.deleteHtmlInput(this.newTab.htmlInput.id).subscribe();
      })
    }
    else {
      this.offersService.updateOffer(this.selectedTab).subscribe(ress => {
        this.toastrService.success("Pomyślnie zaktualizowano ofertę.");
      })
    }
  }

  removeOffer(tab: SingleOfferTabContent){
    if(this.addingNewTab) {
      if(this.newTab.image)
        this.galleryService.deletePhotoWithTarget(this.newTab.image);
      this.clearAndHideNewOfferTab();
    } else {
      this.offersService.deleteOffer(tab.id).subscribe( resp => {
        let index = this.tabs.indexOf(tab);
        this.tabs.splice(index, 1);
        this.toastrService.success("Pomyślnie usunięto ofertę.");
      }) 
    }
  }

  clearAndHideNewOfferTab() {
    this.newTab = new SingleOfferTabContent();
    this.addingNewTab = false;
    this.canAddNewTab = true;
  }

}
