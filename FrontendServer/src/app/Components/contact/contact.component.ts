import { Component, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ContactParam } from 'src/app/Models/Contact/contact-param';
import { ContactParamComponent } from './contact-param/contact-param/contact-param.component';
import { HtmlInput } from 'src/app/Models/htmlInput';
import { PhotoWithTargetDto } from 'src/app/Models/Photos/photo-with-target-dto';
import { HtmlInputsService } from 'src/app/services/html-inputs.service';
import { ContactService } from 'src/app/services/contact.service';
import { GalleryService } from 'src/app/services/gallery.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  @ViewChild('contactCompDiv', {read: ViewContainerRef}) contactCompDiv: ViewContainerRef
  public newContactComponentRef: ComponentRef<ContactParamComponent>;
  
  public contactParams : ContactParam[];
  public canAddNewContact: boolean = true;
  
  //#region About Us - image and html inputs
  public aboutUsHtmlInput: HtmlInput;
  public aboutUsHtmlContentPreview: boolean = false;
  public aboutUsHtmlContentPreviewValue: string;
  public infoPanelTarget_AboutUs = "InformationPanelTarget_AboutUs"
  // #endregion
  
  //#region Workers - image and html inputs
  public workersHtmlInput: HtmlInput;
  public workersHtmlContentPreview: boolean = false;
  public workersHtmlContentPreviewValue: string;
  public infoPanelTarget_Workers = "InformationPanelTarget_Workers"
  // #endregion
  
  constructor(
    public contactService: ContactService, 
    public usersService: UsersService, 
    private componentFactoryResolver: ComponentFactoryResolver,
    private htmlInputsService: HtmlInputsService) { }
  
  ngOnInit(){
    this.loadHtmls();
    this.contactService.getContactParamsPromise().then( value => {
      this.contactParams = value
    });
  }

  loadHtmls() {
    this.htmlInputsService.getHtmlInputByTarget(this.infoPanelTarget_AboutUs).then( resp => {
      this.aboutUsHtmlInput = resp;
    })
    this.htmlInputsService.getHtmlInputByTarget(this.infoPanelTarget_Workers).then( resp => {
      this.workersHtmlInput = resp;
    })
  }
  
  trackByMethod(index, item:ContactParam) {
    return item.paramType + item.paramValue;
  }
  
  AddNewContParamComponent(){
    let factory = this.componentFactoryResolver.resolveComponentFactory(ContactParamComponent);
    this.newContactComponentRef = this.contactCompDiv.createComponent(factory);
    this.newContactComponentRef.instance.NewMode = true;
    this.newContactComponentRef.instance.DestroyComponent = this.DestroyNewContParamComponent;
    
    this.canAddNewContact = false;
  }
  
  DestroyNewContParamComponent = () => {
    this.newContactComponentRef.destroy();
    this.canAddNewContact = true;
  } 
}
