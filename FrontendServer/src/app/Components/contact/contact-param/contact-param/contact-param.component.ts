import { ToastrService } from 'ngx-toastr';
import { ContactParam } from './../../../../Models/Contact/contact-param';
import { Component, Input, OnInit } from '@angular/core';
import { ContactParamEmail, ContactParamFacebook, ContactParamPhone, ContactParamTypes } from 'src/app/StaticParams/ConstValues';
import { NgForm } from '@angular/forms';
import { ContactService } from 'src/app/services/contact.service';
import { UsersService } from 'src/app/services/users.service';



@Component({
  selector: 'contact-param',
  templateUrl: './contact-param.component.html',
  styleUrls: ['./contact-param.component.css']
})
export class ContactParamComponent {
  //#region Properties
  contactParamFacebook = ContactParamFacebook;
  contactParamEmail = ContactParamEmail;
  contactParamPhone = ContactParamPhone;
  contactParamTypes = ContactParamTypes;

  ParamType : string;
  ParamValue : any;
  ParamId: number;
  
  // - #ngForm
  editParamValue : string;
  editSelectedValue: any;

  @Input("ContactParam") 
        get ContactParam(){
          return this._contactParam;
        }
        set ContactParam(value : ContactParam){
          this._contactParam = value;
          this.ParamType = value.paramType;
          this.ParamValue = value.paramValue;
          this.ParamId = value.id;
        }
  @Input("NewMode") NewMode: boolean = false;
  @Input("EditMode") EditMode: boolean = false;
  @Input("DestroyComponent") DestroyComponent: Function
  //#endregion  

  constructor(public usersService: UsersService, public contactService: ContactService, public toastrService: ToastrService) { }
  
  editValuesClick(){
    this.EditMode = true;
    this.editParamValue = this.ParamValue;
    this.editSelectedValue = this.ParamType;
  }

  confirmEditionClick(f: NgForm){
    if(this.contactParamFieldEmptyOrNew()){
      this._contactParam = new ContactParam();
      this.assignFormFieldsIntoParamObject();
      this.contactService.addContact(this.ContactParam);
      
      this.DestroyComponent();
    }
    else{
      this.assignFormFieldsIntoParamObject();
      this.contactService.updateContact(this.ContactParam);
    }
    this.EditMode = false;
  }

  deleteParametrClick(){
    if(this.NewMode) {
        this.DestroyComponent();
        return 0;
      }
    if(confirm("Czy na pewno chcesz usunąć parametr kontaktu?")){
      this.contactService.deleteContact(this.ContactParam);
    }
  }

  cancelEditionClick(){
    if(this.NewMode) {
      this.DestroyComponent();
      return 0;
    }
    this.clearFormAndResetMode();
  }

  private assignFormFieldsIntoParamObject() {
    this._contactParam.paramType = this.editSelectedValue;
    this._contactParam.paramValue = this.editParamValue;
  }

  private clearFormAndResetMode() {
    this.EditMode = false;
    this.editParamValue = null;
    this.editSelectedValue = null;
  }
  
  private contactParamFieldEmptyOrNew: Function = () => (!this._contactParam || !this._contactParam.id);

  private _contactParam: ContactParam;
}


