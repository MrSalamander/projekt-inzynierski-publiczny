import { PhotoWithTargetDto } from './../../../Models/Photos/photo-with-target-dto';
import { NgModule, Component, Input} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AddPhotoToAlbumDto } from 'src/app/Models/Photos/add-photo-to-album-dto';
import { GalleryService } from 'src/app/services/gallery.service';

@Component({
  selector: 'photo-uploader',
  templateUrl: './photo-upload.component.html',
  styleUrls: ['./photo-upload.component.css']
})
export class PhotoUploadComponent {
  @Input("albumId") albumId : number;
  /** Syntax in PascalCase, default: GalleryAlbum */
  @Input("target") target: string = "GalleryAlbum"; 
  @Input("targetWidth") targetWidth: number;
  @Input("askForComment") askForComment: boolean = true;
  @Input("disabled") disabled: boolean = false;

  @Input() reloadCallbackFunction: () => void = () => {};
  @Input() reloadCallbackFunctionWithPayload: (photoWithTargetDto: PhotoWithTargetDto) => void = () => {};

  newPhoto: AddPhotoToAlbumDto = new AddPhotoToAlbumDto();
  
  constructor(private galleryService: GalleryService, private toastr: ToastrService) { }

  uploadPhoto(files){
    this.newPhoto.file = files[0];

    if(this.newPhoto.file.size == 0)
      this.toastr.error("Wybrany plik jest pusty");
    else
      this.toastr.success("Pomyślnie zaimportowano plik");
  }

  saveBtnClicked(){
    if(!this.newPhoto.file)
    {
      this.toastr.warning("Należy najpierw wgrać plik.")
      return;
    }

    switch(this.target)
    {
      case "GalleryAlbum": {
          this.addPhotoToAlbum();
          break;
      }
      default: {
        this.addPhotoWithDifferentTarget();
      }
    }

    this.newPhoto = new AddPhotoToAlbumDto();
  }

  private addPhotoToAlbum(){
    let formData: FormData = new FormData();
    formData.append('file', this.newPhoto.file, this.newPhoto.file.name);
    if(!this.newPhoto.comment) this.newPhoto.comment = " ";
    formData.append('comment', this.newPhoto.comment);
    formData.append('albumId', this.albumId.toString());
    
    this.galleryService.addPhotoToAlbum(formData);
  }
  
  private addPhotoWithDifferentTarget() {
    let formData: FormData = new FormData();
    formData.append('file', this.newPhoto.file, this.newPhoto.file.name);
    if(!this.newPhoto.comment) this.newPhoto.comment = " ";
    formData.append('comment', this.newPhoto.comment);
    formData.append('target', this.target);
    formData.append('targetWidth', this.targetWidth.toString());

    this.galleryService.addPhotoWithTarget(formData).subscribe(newlyAddedPhoto => {
      this.toastr.success("Pomyślnie dodano zdjęcie");
      this.reloadCallbackFunction();
      this.reloadCallbackFunctionWithPayload(newlyAddedPhoto);
      });;
  }
}
