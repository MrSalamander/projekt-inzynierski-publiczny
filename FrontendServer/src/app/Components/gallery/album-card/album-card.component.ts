import { Component, Input} from '@angular/core';
import { PhotoAlbumReturnDto } from 'src/app/Models/Gallery/photo-album-return-dto';
import { PhotoFromAlbumDto } from 'src/app/Models/Photos/photo-from-album-dto';
import { GalleryService } from 'src/app/services/gallery.service';
import { UsersService } from 'src/app/services/users.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss']
})
export class AlbumCardComponent {
  @Input() album: PhotoAlbumReturnDto;
  public selectedImage?: number = null;

  name : string;
  comment : string;
  
  constructor(private galleryService: GalleryService, public usersService: UsersService, private modalService: NgbModal) { }

  updateAlbum(){
    this.album.name = this.name;
    this.album.comment = this.comment;
    this.galleryService.updateAlbum({id: this.album.id, comment: this.album.comment, name: this.album.name});
  }

  removePhotoClick(photo: PhotoFromAlbumDto){  
    //usunięcie przez serwis
    this.galleryService.deletePhotoFromAlbum(photo);
  }

  removeAlbumClick(album: PhotoAlbumReturnDto){
    if (confirm("Potwierdzenie spowoduje trwałe usunięcie albumu: " + album.name + "\n\nCzy chcesz kontynuować?"))
      this.galleryService.deleteAlbum(album.id);
  }
  
  open(content) {
    this.name = this.album.name;
    this.comment = this.album.comment;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.updateAlbum();
    });

    
  }

  setSource(element, url) {
    element.src = url;
 }





}
