import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PhotoAlbumAddDto } from 'src/app/Models/Gallery/photo-album-add-dto';
import { PhotoAlbumReturnDto } from 'src/app/Models/Gallery/photo-album-return-dto';
import { GalleryService } from 'src/app/services/gallery.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})

export class GalleryComponent implements OnInit {
    // Adding new album props
    public name: string;
    public comment: string;
    
    public galleryAlbums: PhotoAlbumReturnDto[] = [];
    public selectedAlbumId: number;
    public selectedAlbum: PhotoAlbumReturnDto;

  constructor(
    public route: ActivatedRoute, 
    public usersService: UsersService, 
    public galleryService: GalleryService, 
    private modalService: NgbModal) { 
        route.queryParamMap.subscribe(params => {
            this.selectedAlbumId = Number(params.get("selectedAlbumId"))
            this.selectedAlbum = this.galleryAlbums.find(x => x.id == this.selectedAlbumId);
          })
    }

  ngOnInit(): void {
      this.loadAlbums();
      
  }

  loadAlbums(){
    this.galleryService.getAlbumsPromise()
    .then(resp => {
        this.galleryAlbums = resp;
    })
    .finally( () => {
        this.selectedAlbumId = this.galleryAlbums?.length > 0 ? this.galleryAlbums[0].id : null;
        this.selectedAlbum = this.galleryAlbums.find(x => x.id == this.selectedAlbumId);    
    })
  }

  addAlbum(){
    let album: PhotoAlbumAddDto = {name: this.name, comment: this.comment}
    this.galleryService.addAlbum(album);
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.addAlbum();
    });
  }

  //#region mockupy
//   public getAlbums(): Observable<PhotoAlbumReturnDto[]> {
//     const mocked: PhotoAlbumReturnDto[] = [
//      {
//       "id": 15,
//       "photos": [
//           {
//               "photoAlbumId": 15,
//               "id": 44,
//               "comment": "Testowe zdjęcie 1 ",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613471395/tjdyvby3szbmudimrgfm.jpg"
//           },
//           {
//               "photoAlbumId": 15,
//               "id": 52,
//               "comment": "Testowe zdjęcie 2",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613574762/xgxyvogsgmfltfhoa1ps.jpg"
//           },
//           {
//               "photoAlbumId": 15,
//               "id": 53,
//               "comment": "Testowe zdjęcie 3",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585366/emrabad0fxde28ofjkdl.jpg"
//           },
//           {
//               "photoAlbumId": 15,
//               "id": 54,
//               "comment": "Testowe zdjęcie 4",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585387/n4337tirnarg9mjjt3v0.jpg"
//           },
//           {
//               "photoAlbumId": 15,
//               "id": 55,
//               "comment": "Testowe zdjęcie 5",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585398/rz6lqbz5zqbw2u1v8jld.jpg"
//           },
//           {
//             "photoAlbumId": 15,
//             "id": 44,
//             "comment": "Testowe zdjęcie 1 ",
//             "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613471395/tjdyvby3szbmudimrgfm.jpg"
//         },
//         {
//             "photoAlbumId": 15,
//             "id": 52,
//             "comment": "Testowe zdjęcie 2",
//             "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613574762/xgxyvogsgmfltfhoa1ps.jpg"
//         },
//         {
//             "photoAlbumId": 15,
//             "id": 53,
//             "comment": "Testowe zdjęcie 3",
//             "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585366/emrabad0fxde28ofjkdl.jpg"
//         },
//         {
//             "photoAlbumId": 15,
//             "id": 54,
//             "comment": "Testowe zdjęcie 4",
//             "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585387/n4337tirnarg9mjjt3v0.jpg"
//         },
//         {
//             "photoAlbumId": 15,
//             "id": 55,
//             "comment": "Testowe zdjęcie 5",
//             "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585398/rz6lqbz5zqbw2u1v8jld.jpg"
//         },
//         {
//           "photoAlbumId": 15,
//           "id": 44,
//           "comment": "Testowe zdjęcie 1 ",
//           "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613471395/tjdyvby3szbmudimrgfm.jpg"
//       },
//       {
//           "photoAlbumId": 15,
//           "id": 52,
//           "comment": "Testowe zdjęcie 2",
//           "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613574762/xgxyvogsgmfltfhoa1ps.jpg"
//       },
//       {
//           "photoAlbumId": 15,
//           "id": 53,
//           "comment": "Testowe zdjęcie 3",
//           "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585366/emrabad0fxde28ofjkdl.jpg"
//       },
//       {
//           "photoAlbumId": 15,
//           "id": 54,
//           "comment": "Testowe zdjęcie 4",
//           "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585387/n4337tirnarg9mjjt3v0.jpg"
//       },
//       {
//           "photoAlbumId": 15,
//           "id": 55,
//           "comment": "Testowe zdjęcie 5",
//           "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585398/rz6lqbz5zqbw2u1v8jld.jpg"
//       }
//       ],
//       "name": "Test - środa 17",
//       "comment": "Tutaj będzie fajny komentarz"
//      } ,
//      {
//       "id": 16,
//       "photos": [
//           {
//               "photoAlbumId": 16,
//               "id": 1,
//               "comment": "Testowe zdjęcie 1 ",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613471395/tjdyvby3szbmudimrgfm.jpg"
//           },
//           {
//               "photoAlbumId": 16,
//               "id": 2,
//               "comment": "Testowe zdjęcie 2",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613574762/xgxyvogsgmfltfhoa1ps.jpg"
//           },
//           {
//               "photoAlbumId": 16,
//               "id": 3,
//               "comment": "Testowe zdjęcie 3",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585366/emrabad0fxde28ofjkdl.jpg"
//           },
//           {
//               "photoAlbumId": 16,
//               "id": 4,
//               "comment": "Testowe zdjęcie 4",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585387/n4337tirnarg9mjjt3v0.jpg"
//           },
//           {
//               "photoAlbumId": 16,
//               "id": 5,
//               "comment": "Testowe zdjęcie 5",
//               "path": "https://res.cloudinary.com/dzz4ptzs6/image/upload/v1613585398/rz6lqbz5zqbw2u1v8jld.jpg"
//           }
//       ],
//       "name": "Test - czw 18",
//       "comment": "Tutaj będzie fajny komentarz"
//      } 
  
     
//     ];
//     // returns an Observable that emits one value, mocked; which in this case is an array,
//     // and then a complete notification
//     // You can easily just add more arguments to emit a list of values instead
//     return of(mocked);
//   }
  //#endregion
}
