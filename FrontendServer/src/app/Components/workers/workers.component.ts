import { permissionLevelStarszyEdytor } from './../../StaticParams/permissions';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { WorkerReturnDto } from '../../Models/Workers/worker-return-dto';
import { WorkersService } from 'src/app/services/workers.service';
import { UsersService } from 'src/app/services/users.service';


@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.scss']
})
export class WorkersComponent implements OnInit {
  public workers: WorkerReturnDto[];
  public workersPermissionLevelNeeded = permissionLevelStarszyEdytor;

  public alternativeImg = "https://st.depositphotos.com/2101611/3925/v/600/depositphotos_39258143-stock-illustration-businessman-avatar-profile-picture.jpg"
  public isEditMode: boolean;

  //#region modal form models
  public name: string;
  public rank: string;
  public workingFrom: Date;
  public currentDate: Date = new Date();
  public profilePhoto: File;
  //#endregion

  //#region Ctor
  constructor(public workersService: WorkersService,
    public usersService: UsersService,
    private modalService: NgbModal,
    private toastr: ToastrService) { }
    
  ngOnInit(): void {
    this.workersService.getWorkers().subscribe(workers => { this.workers = workers});
  }
  //#endregion

  openModal(content, worker: WorkerReturnDto) {
    if (this.usersService.userHasStarszyEdytorPermission) {
      if (!this.isEditMode) {
        this.name = null;
        this.rank = null;
      }

      if (this.isEditMode)
      {
        this.name = worker.name;
        this.rank = worker.rank;
        this.workingFrom = new Date(worker.workingFrom);
      }
      this.openModalWithService(content, worker)
    }
    else {
      this.toastr.warning("Brak uprawnień");
    }
  }

  deleteWorker(worker: WorkerReturnDto) {
    let confirmationAnswer = confirm("Potwierdzenie spowoduje trwałe usunięcie profilu pracownika: " + worker.name + "\n\nCzy chcesz kontynuować?");
    if (this.usersService.userHasStarszyEdytorPermission && confirmationAnswer){
        this.workersService.deleteWorker(worker.id).subscribe(resp => {
          let index = this.workers.indexOf(worker);
          this.workers.splice(index, 1);
          this.toastr.success("Pomyślnie usunięto pracownika.")
        });
      }
    else
      this.toastr.warning("Brak uprawnień");
  }

  deleteWorkerPhoto(worker: WorkerReturnDto) {
    if (this.usersService.userHasStarszyEdytorPermission)
      if (confirm("Potwierdzenie spowoduje trwałe usunięcie zdjęcie profilowe pracownika: " + worker.name + "\n\nCzy chcesz kontynuować?"))
        this.workersService.deleteWorkerPhoto(worker.id).subscribe(resp => {
          worker.profilePhoto = null;
          this.toastr.success("Pomyślnie usunięto zdjęcie profilowe pracownia.")
        });
    else
      this.toastr.warning("Brak uprawnień");
  }

  loadPhoto(event) {
    this.profilePhoto = event[0];
    this.toastr.success("załadowano zdjęcie");
  }

  private openModalWithService(content, worker: WorkerReturnDto){
    this.modalService.open(content, { windowClass: 'dark-modal' }).result.then((result) => {
      let formData: FormData = new FormData();
      formData.append('photo', this.profilePhoto);
      formData.append('name', this.name);
      formData.append('rank', this.rank);
      if (this.workingFrom)
        formData.append('workingFrom', new Date(this.workingFrom).toJSON());

      if (!this.isEditMode)
        this.addNewWorkerWithService(formData);
      if (this.isEditMode) {
        this.updateWorkerWithService(formData, worker);
      }
    }, (reason) => {});
  }
  
  private updateWorkerWithService(formData: FormData, worker: WorkerReturnDto) {
    formData.append('id', worker.id.toString());
    this.workersService.updateWorker(formData).subscribe(resp => {
      worker.name = resp.name;
      worker.rank = resp.rank;
      worker.workingFrom = resp.workingFrom;
      worker.withUsFor = resp.withUsFor;
      worker.profilePhoto = resp.profilePhoto;
      this.toastr.success("Pomyślnie zaktualizowano dane pracownika.");
    });
  }

  private addNewWorkerWithService(formData: FormData ){
    this.workersService.addWorker(formData).subscribe( resp => {
      if (resp) {
        this.workers.push(resp);
        this.toastr.success("Pomyślnie dodano pracownika.");
      }})
  }

  onImgError(source) {
    source.target.src = this.alternativeImg;
    source.onerror = "";
    return true;
  }

  trackByMethod(index, item:WorkerReturnDto) {
    return item && item.name && item.rank && item.workingFrom && item.profilePhoto?.path && item.profilePhoto && item.profilePhoto.id;
  }
}
