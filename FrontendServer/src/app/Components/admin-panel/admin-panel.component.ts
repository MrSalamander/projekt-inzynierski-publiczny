import { permissionLevelMlodszyEdytor, permissionLevelStarszyEdytor } from './../../StaticParams/permissions';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ChangeCurrentUserPasswordDto } from 'src/app/Models/Users/change-current-user-password-dto';
import { RegisterUserDto } from 'src/app/Models/Users/register-user-dto';
import { SimpleUserDto } from 'src/app/Models/Users/simple-user-dto';
import { UsersService } from 'src/app/services/users.service';
import { permissionLevelAdmin } from 'src/app/StaticParams/permissions';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
  public isPasswordChange: boolean;
  public isUserAdd: boolean;
  public isRoleChange: boolean;

  //#region modal properties
  selectedPermission: string;
  username: string;
  newPass: string;
  newPass2: string;
  oldPass: string;
  //#endregion

  public mlodszyEdytorPermission = permissionLevelMlodszyEdytor;
  public starszyEdytorPermission = permissionLevelStarszyEdytor;
  public adminPermission = permissionLevelAdmin;
  
  private modalFinishOperation : Function = () => {};
  // #region Ctor
  constructor(
    public usersService: UsersService,
    private toastr: ToastrService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.isPasswordChange = false;
    this.isUserAdd = false;
    this.isRoleChange = false;

    this.usersService.getUsers().subscribe();
  }
  // #endregion 

  openAddNewUserModal(content) {
    this.isUserAdd = true;
    this.selectedPermission = "Admin";
    this.modalFinishOperation = this.registerUser;
    this.openProperModal(content)
  }

  openChangePasswordModal(content) {
    this.isPasswordChange = true;
    this.modalFinishOperation = this.changePassword;
    this.openProperModal(content)
  }

  openChangePermissionModal(content, user?: SimpleUserDto) {
    this.isRoleChange = true;
    this.username = user.username;
    this.selectedPermission = user.permissionLevelName;
    this.modalFinishOperation = this.changePermission;
    this.openProperModal(content)
  }

  deleteUser(username: string) {
    if (confirm("Potwierdzenie spowoduje trwałe usunięcie konta użytkownika " + username + "\n\nCzy chcesz kontynuować?")) {
      if (this.usersService.isCurrentOrAdmin(username)) {
        this.toastr.warning("Operacja niedozwolona");
        return;
      }
      this.usersService.deleteUser(username);
    }
  }

  setPermission(permission: string) {
    this.selectedPermission = permission;
  }

  private openProperModal(content){
    this.modalService.open(content, { windowClass: 'dark-modal' }).result.then((result) => {
      this.modalFinishOperation();
      this.clearStatus();
    }, (reason) => {
      this.clearStatus();
    });
  }
  
  private registerUser() {
    if(!this.passwordVerifiedPositive())
      return;

    let newUser: RegisterUserDto = {
      username: this.username,
      password: this.newPass,
      permissionName: this.selectedPermission
    }
    this.usersService.register(newUser);
  }

  private changePassword() {
    if(!this.passwordVerifiedPositive())
      return;
    let passDto: ChangeCurrentUserPasswordDto = { currentPassword: this.oldPass, newPassword: this.newPass }
    this.usersService.updateMyPassword(passDto).subscribe(resp => { this.toastr.success(resp) });
  }
  
  private changePermission() {
    if (this.usersService.isCurrentOrAdmin(this.username)) {
      this.toastr.warning("Operacja niedozwolona");
      return;
    }
    let payload: SimpleUserDto = { username: this.username, permissionLevelName: this.selectedPermission };
    this.usersService.updateUserPermission(payload);
  }
  
  private clearStatus() {
    this.isPasswordChange = false;
    this.isUserAdd = false;
    this.isRoleChange = false;
  }

  private passwordVerifiedPositive() : boolean{
    if (this.newPass !== this.newPass2) {
      this.toastr.error("Hasła muszą być takie same");
      return false;
    }
    return true;
  }
}
