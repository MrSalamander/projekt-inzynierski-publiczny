import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUserDto } from 'src/app/Models/Users/login-user-dto';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{
  model: LoginUserDto = new LoginUserDto();

  constructor(public usersService: UsersService, private router: Router) { }

  login() {
    this.usersService.login(this.model).subscribe(resp => {
      if(this.usersService.userHasAdminPermission)
        this.router.navigateByUrl('/admin-panel');
      else
        this.router.navigateByUrl('');
    });
  }
}
