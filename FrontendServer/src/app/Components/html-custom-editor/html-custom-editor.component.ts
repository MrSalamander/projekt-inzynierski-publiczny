import { throwError } from 'rxjs';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { editorConfig } from 'src/app/StaticParams/Kolkov-editor-configuration';
import { HtmlInput } from './../../Models/htmlInput';
import { ToastrService } from 'ngx-toastr';
import { HtmlInputsService } from 'src/app/services/html-inputs.service';

@Component({
  selector: 'html-custom-editor',
  templateUrl: './html-custom-editor.component.html',
  styleUrls: ['./html-custom-editor.component.scss']
})
export class HtmlCustomEditorComponent {
  _htmlInput: HtmlInput = new HtmlInput();
  @Input() set htmlInput(value: HtmlInput){
    this._htmlInput = value;
    this.htmlWorkingContent = value?.value;
    
  }
  get htmlInput() { return this._htmlInput }
  @Output() htmlInputChange: EventEmitter<HtmlInput> = new EventEmitter<HtmlInput>();

  @Input() target: string;

  @Input() htmlWorkingContent: string;
  @Output() htmlWorkingContentChange: EventEmitter<string> = new EventEmitter<string>();
  htmlContentEdited(newValue) { this.htmlWorkingContentChange.emit(newValue); }

  @Input() showPreviewOutside: boolean = false;
  @Output() showPreviewOutsideChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  togglePreviewCheckbox(newValue) { this.showPreviewOutsideChange.emit(this.showPreviewOutside); }

  @Input() acceptClickCallback: Function;
  @Input() alternativeRemoveButtonClickImplementation: (htmlInput: HtmlInput) => void;

  config = editorConfig; 

  //#region ctor
  constructor( public htmlInputsService: HtmlInputsService, private toastrService: ToastrService) { }
  //#endregion

  confirmHtmlContentEdition() {
    if(this.htmlInput?.id) {
      let tmpValHolder = this.htmlInput.value;
      this.htmlInput.value = this.htmlWorkingContent
      this.htmlInputsService.updateHtmlInput(this.htmlInput).subscribe( resp => {
      this.htmlInput = resp;
      this.toastrService.success("Pomyślnie zaktualizowano sekcję.")
      this.htmlInputChange.emit(this.htmlInput);
      if(this.acceptClickCallback) this.acceptClickCallback()
      }, error => {
        this.toastrService.error(error)
        this.htmlInput.value = tmpValHolder
        throwError(error);
      })
    }
    else {  // new input
      this._htmlInput = new HtmlInput();
      this.htmlInput.target = this.target
      this.htmlInput.value = this.htmlWorkingContent
      this.htmlInputsService.addHtmlInput(this.htmlInput).subscribe( resp => {
        this.htmlInput = resp;
        this.toastrService.success("Pomyślnie dodano sekcję.")
        this.htmlInputChange.emit(this.htmlInput);
        if(this.acceptClickCallback) this.acceptClickCallback()
      })
    }
  }

  removeHtmlContent(){
    if(confirm("Czy na pewno chcesz skasować obecną sekcję?")) {
      if(!this.alternativeRemoveButtonClickImplementation){
        if(this.htmlInput?.id)
          this.htmlInputsService.deleteHtmlInput(this.htmlInput.id).subscribe( resp => {
            this.htmlInput = null
            this.htmlInputChange.emit(this.htmlInput);
            this.toastrService.success("Pomyślnie usunięto sekcję.")
          })
      } else this.alternativeRemoveButtonClickImplementation(this.htmlInput);
    }
  }
}
