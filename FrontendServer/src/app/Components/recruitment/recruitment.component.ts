import { HtmlInput } from './../../Models/htmlInput';
import { EmployOfferComponent } from './employ-offer/employ-offer.component';
import { ToastrService } from 'ngx-toastr';
import { Component, ComponentFactoryResolver, ComponentRef, Input, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { EmploymentOffer } from '../../Models/EmploymentOffers/employmentOffer';
import { HtmlInputsService } from 'src/app/services/html-inputs.service';
import { EmploymentOffersService } from 'src/app/Services/employmentOffers.service';
import { UsersService } from 'src/app/services/users.service';



@Component({
  selector: 'app-recruitment',
  templateUrl: './recruitment.component.html',
  styleUrls: ['./recruitment.component.css']
})
export class RecruitmentComponent implements OnInit {
  @ViewChild('newOfferCompContainer', {read: ViewContainerRef}) newOfferCompContainer: ViewContainerRef
  public NewEmployOfferComponentRef: ComponentRef<EmployOfferComponent>;
  
  @Input("canAddNewOffer") public canAddNewOffer: boolean = true;
  public offers: EmploymentOffer[] = [];
  addNewOfferCallback = (offer: EmploymentOffer) => this.addNewOffer(offer);
  removeOfferCallback = (offer: EmploymentOffer) => this.deleteOfferFromArray(offer); 
  
  // htmlContentEdition
  public recruitmentCompTarget_1 = "recruitmentCompTarget_1";
  public htmlContentPreviewValue: string = '';
  public htmlContentPreview = false;

  public htmlInput: HtmlInput;

  //#region Ctor
  constructor(
    public usersService: UsersService, 
    private componentFactoryResolver: ComponentFactoryResolver,
    private offersService: EmploymentOffersService,
    private htmlInputsService: HtmlInputsService,
    private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.loadOffers();
    this.initializeHtmlInput();
  }

  loadOffers() {
    this.offersService.getEmploymentOffers().subscribe( offersCollection => {
      this.offers = offersCollection;
    })
  }

  initializeHtmlInput() {
    this.htmlInputsService.getHtmlInputByTarget(this.recruitmentCompTarget_1).then(resp => {
      this.htmlInput = resp;
    })
  }
  //#endregion
  
  //#region offers
  addNewOffer(offer: EmploymentOffer) {
    this.offers.push(offer);
  }
  
  deleteOfferFromArray(offer: EmploymentOffer) {
    let index = this.offers.indexOf(offer);
    this.offers.splice(index, 1);
  }
  //#endregion

  // #region dynamic menagement (adding new offer comp)
  addNewEmployOfferComponent() {
    let factory = this.componentFactoryResolver.resolveComponentFactory(EmployOfferComponent);
    this.NewEmployOfferComponentRef = this.newOfferCompContainer.createComponent(factory);
    this.NewEmployOfferComponentRef.instance.newOfferMode = true;
    this.NewEmployOfferComponentRef.instance.addNewOfferCallback = this.addNewOfferCallback;
    this.NewEmployOfferComponentRef.instance.destroyComponent = this.destroyNewEmployOfferComponent;
    
    this.canAddNewOffer = false;
  }

  destroyNewEmployOfferComponent = () => {
    this.NewEmployOfferComponentRef.destroy();
    this.canAddNewOffer = true;
  }
  //#endregion
  
}
