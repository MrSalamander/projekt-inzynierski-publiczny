import { HtmlInput } from './../../../Models/htmlInput';
import { PhotoWithTargetDto } from './../../../Models/Photos/photo-with-target-dto';
import { EmploymentOffer } from '../../../Models/EmploymentOffers/employmentOffer';
import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { GalleryService } from 'src/app/services/gallery.service';
import { EmploymentOffersService } from 'src/app/Services/employmentOffers.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'employ-offer',
  templateUrl: './employ-offer.component.html',
  styleUrls: ['./employ-offer.component.scss']
})
export class EmployOfferComponent {
  AddOfferAndGenerateProperTarget(): string {
    throw new Error('Method not implemented.');
  }
  @Input("offer") 
  set offer(value: EmploymentOffer){
    this._offer = value;
    this.recruitmentOfferTarget = 'RecruitmentOffer_' + this.offer?.id
  } 
  get offer(){ return this._offer }
  
  @Input("newOffer") 
  set newOfferMode(value: boolean){ 
    this._newOfferMode = value; 
    if(value)
      this.offer = new EmploymentOffer();
  }
  get newOfferMode(){ return this._newOfferMode }
  
  @Input("addNewOfferCallback") addNewOfferCallback = (newOffer: EmploymentOffer): void => {}
  @Input("removeOfferCallback") removeOfferCallback = (newOffer: EmploymentOffer): void => {}
  @Input("destroyComponent") destroyComponent: Function
  reloadCallbackFunctionWithPayload = (photo: PhotoWithTargetDto): void => this.updateOfferPhotoCallback(photo);

  acceptClickCallback = (): void => this.confirmEdition();
  alternativeRemoveButtonClickImplementation = (htmlInput: HtmlInput): void => this.removeOffer();
  public recruitmentOfferTarget;


  htmlContentPreview : boolean = false;
  htmlContentPreviewValue: string = " ";
  
  private _offer = new EmploymentOffer();
  private _newOfferMode = false;

  constructor(
    public usersService: UsersService,
    private offersService: EmploymentOffersService,
    private galleryService: GalleryService,
    private toastrService: ToastrService) { }
    
  public confirmEdition() {
    if(this.newOfferMode)
      this.addNewOffer();
    else
      this.updateOffer();
  }

  addNewOffer() {
    this.offersService.addEmploymentOffer(this.offer).subscribe((newOffer) => {
      this.toastrService.success("Pomyślnie dodano nową ofertę pracy.");
      this.offer.id = newOffer.id;
      this.offer.htmlInput.id = newOffer.htmlInput.id;
      this.addNewOfferCallback(this.offer);
      this.destroyComponent();
    });
  }

  updateOffer() {
    this.offersService.updateEmploymentOffer(this.offer).subscribe(() => {
      this.toastrService.success("Pomyślnie zaktualizowano ofertę pracy.");
    });
  }

  removeOffer(){
    if(this.newOfferMode){
      if(this.offer.photo)
        this.removeOfferPhoto();
        this.destroyComponent();
    }
    else {
      console.log(this.offer)
      this.offersService.deleteEmploymentOffer(this.offer).subscribe(() => {
        this.toastrService.success("Pomyślnie usunięto ofertę pracy.")
        this.removeOfferCallback(this.offer);
      })
    }
  }
    
  removeOfferPhoto() {
    if(this.newOfferMode) {
      this.galleryService.deletePhotoWithTarget(this.offer.photo).subscribe( resp => {
        this.offer.photo = null;
        this.toastrService.success("Pomyślnie usunięto zdjęcie z oferty.");
      });
    } else {
      this.offersService.deleteEmploymentOfferPhoto(this.offer.id).subscribe( resp => {
        this.offer.photo = null;
        this.toastrService.success("Pomyślnie usunięto zdjęcie z oferty.");
      });
    }
  }

  private updateOfferPhotoCallback(photo: PhotoWithTargetDto) {
    let oldPhoto = this.offer.photo;
    this.offer.photo = photo;
    if(!this.newOfferMode) { 
      this.offersService.updateEmploymentOffer(this.offer).subscribe( resp => {
        this.toastrService.success("Pomyślnie zaktualizowano zdjęcie oferty");
      }, error => {
        this.offer.photo = oldPhoto;
      })
    }   
  }
}
