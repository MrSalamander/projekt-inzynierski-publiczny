import { Component, OnInit } from '@angular/core';
import  * as settingsJson from 'src/assets/CalculatorSettings.json'
import { CalcSettings } from './settings';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  choice = 1;
  ileMetrow: number;
  obwodBudynku: number;
  glebokoscFundamentu: number;
  cenaKostki: number;

  result: number;

  settings : CalcSettings = (settingsJson as any).default;

  constructor() { }

  ngOnInit(): void {    
  }

  calculate(){
    var result = 0;
      if(this.choice == 1){
        result = this.calculateFundament();
      }
      if(this.choice == 2){
        result = this.calculateOdwodnienie();
      }
      if(this.choice == 3){
        result = this.calculateKostka();
      }
      this.result = result;
  }

  calculateFundament() : number{
    return this.settings.fundament * this.ileMetrow;
  }

  calculateOdwodnienie()  : number{
    return this.obwodBudynku * this.glebokoscFundamentu * this.settings.odwodnienie;
  }

  calculateKostka() : number{
    return this.ileMetrow * this.cenaKostki * this.settings.kostka;
  }
}
