import { HtmlInput } from './../../Models/htmlInput';
import { PhotoWithTargetDto } from './../../Models/Photos/photo-with-target-dto';
import { ContactParamComponent } from './../contact/contact-param/contact-param/contact-param.component';
import { Component, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ContactParam } from 'src/app/Models/Contact/contact-param';
import { HtmlInputsService } from 'src/app/services/html-inputs.service';
import { GalleryService } from    'src/app/services/gallery.service';
import { ContactService } from    'src/app/services/contact.service';
import { UsersService } from      'src/app/services/users.service';


@Component({
  selector: 'app-home-panel',
  templateUrl: './home-panel.component.html',
  styleUrls: ['./home-panel.component.scss']
})
export class HomePanelComponent implements OnInit {
  @ViewChild('contactCompDiv', {read: ViewContainerRef}) contactCompDiv: ViewContainerRef

  reloadCallbackFunction = (): void => this.loadImages();

  public readonly HomePanelUpperImage = "HomePanelUpperImage";
  public readonly HomePanelCarouselImage_ = "HomePanelCarouselImage_";
    
  public upperImage: PhotoWithTargetDto;
  public images: PhotoWithTargetDto[] = [];
  public carouselImages: PhotoWithTargetDto[] = [];
  
  public contactParams : ContactParam[];
  public NewContactComponentRef: ComponentRef<ContactParamComponent>;
  public canAddNewContact: boolean = true;

  public mainHtmlInput: HtmlInput;
  public htmlContentPreview: boolean = false;
  public htmlContentPreviewValue: string;
  public mainPanelOfferTarget = "MainPanelOfferTarget"

  constructor(
    public usersService: UsersService, 
    public contactService: ContactService, 
    private galleryService: GalleryService, 
    private htmlInputsService: HtmlInputsService,
    private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
    this.loadContactParams();
    this.loadImages();
    this.loadHtmlInput();
  }
  loadHtmlInput() {
    this.htmlInputsService.getHtmlInputByTarget(this.mainPanelOfferTarget).then(resp => {
      this.mainHtmlInput = resp;
    })
  }
  
  loadContactParams() {
    this.contactService.getContactParamsPromise().then(value => {
      this.contactParams = value
    });
  }
  
  loadImages() {
    this.galleryService.getPhotosWithTargetPromise().then(value => {
      this.images = value;
      this.upperImage = this.findImage(this.HomePanelUpperImage);
      for(let i = 0; i < 4; i++)
        this.carouselImages[i] = this.findImage(this.HomePanelCarouselImage_ + i);
    })  
  }

  trackByMethod(index, item:ContactParam) {
    return item.paramType + item.paramValue;
  }
  trackByMethodCarousel(index, photo: PhotoWithTargetDto) {
    return photo?.id + photo?.comment + photo?.photoTarget;
  }

  findImage(target: string) {
    return this.images.find(x => x.photoTarget === target);
  }

  removePhotoClick(photo: PhotoWithTargetDto, target: string){  
    if (confirm("Czy na pewno chcesz usunąć to zdjęcie?")) {
      this.galleryService.deletePhotoWithTarget(photo).subscribe( resp => {
        if(target == this.HomePanelUpperImage)
        this.upperImage = null;
      else if(target.includes(this.HomePanelCarouselImage_)){
        let index = Number.parseInt(target.split("_").pop());
        this.carouselImages[index] = null;
      }})
    }
  }

  AddNewContParamComponent(){
    let factory = this.componentFactoryResolver.resolveComponentFactory(ContactParamComponent);
    this.NewContactComponentRef = this.contactCompDiv.createComponent(factory);
    this.NewContactComponentRef.instance.NewMode = true;
    this.NewContactComponentRef.instance.DestroyComponent = this.DestroyNewContParamComponent;
    
    this.canAddNewContact = false;
  }

  DestroyNewContParamComponent = () => {
    this.NewContactComponentRef.destroy();
    this.canAddNewContact = true;
  }


  
}
