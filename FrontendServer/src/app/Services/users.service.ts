import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { deleteUserUrl, getUsersUrl, loginUrl, registerUrl, tokenPath, updateCurrentUserPasswordUrl, updateUserPermissionUrl } from 'src/assets/config';
import { ChangeCurrentUserPasswordDto } from '../Models/Users/change-current-user-password-dto';
import { LoggedUserDto } from '../Models/Users/logged-user-dto';
import { LoginUserDto } from '../Models/Users/login-user-dto';
import { RegisterUserDto } from '../Models/Users/register-user-dto';
import { SimpleUserDto } from '../Models/Users/simple-user-dto';
import { permissionLevelAdmin, permissionLevelStarszyEdytor, permissionLevelMlodszyEdytor, permissions } from '../StaticParams/permissions';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  
  constructor(private http: HttpClient, private toastr: ToastrService) { }
  public loggedUserDto: LoggedUserDto;

  //#region Permissions
  private _hasMlodszyEdytorPermission: boolean;
  private _hasStarszyEdytorPermission: boolean;
  private _hasAdminPermission: boolean;

  get loggedIn() { return this.loggedUserDto != null}
  get userHasMlodszyEdytorPermission(){
    if(!this._hasMlodszyEdytorPermission) {
      let res = this.haveAccess(permissionLevelMlodszyEdytor);
      this._hasMlodszyEdytorPermission = res;
    }
    return this._hasMlodszyEdytorPermission;
  }
  get userHasStarszyEdytorPermission(){
    if(!this._hasStarszyEdytorPermission) {
      let res = this.haveAccess(permissionLevelStarszyEdytor);
      this._hasStarszyEdytorPermission = res;
    }
    return this._hasStarszyEdytorPermission;
  }
  get userHasAdminPermission(){
    if(!this._hasAdminPermission) {
      let res = this.haveAccess(permissionLevelAdmin);
      this._hasAdminPermission = res;
    }
    return this._hasAdminPermission;
  }

  private haveAccess(permissionLevelName: string){
    if(!permissions.includes(permissionLevelName))
      throw new Error(permissionLevelName + ' nie został rozpoznany.');
    if(!this.loggedIn) 
      return false;
    if(permissionLevelName)
      return this.firstPermHasOrIsOverSecondPerm(this.loggedUserDto.permissionLevelName, permissionLevelName);
  }
  
  private firstPermHasOrIsOverSecondPerm(firstPerm: string, secondPerm: string) {
    switch(firstPerm){
      case permissionLevelAdmin:
          return true;
      case permissionLevelStarszyEdytor:
        if(secondPerm == permissionLevelAdmin)
          return false;
        else
          return true;
      case permissionLevelMlodszyEdytor:
        if(secondPerm == permissionLevelMlodszyEdytor)
          return true;
        else 
          return false;
    }
  }

  private reloadPermissions(){
    this.loggedUserDto = null; 
    this._hasMlodszyEdytorPermission = null;
    this._hasStarszyEdytorPermission = null;
    this._hasAdminPermission = null;
  }
  //#endregion

  private usersSubject = new ReplaySubject<SimpleUserDto[]>();
  public users$ = this.usersSubject.asObservable();
  
  get token() {
    var tokenTmp =  sessionStorage.getItem(tokenPath);
    if(tokenTmp)
      return tokenTmp.replace('"','').replace('"','');
    
    this.reloadPermissions();
    return " ";
  }
  
  getUsers(){
    return this.http.get<SimpleUserDto[]>(getUsersUrl).pipe( 
      map((resp: SimpleUserDto[]) => {
        let adminIndex = resp.findIndex(x => x.username == "Admin")
        if(adminIndex >= 0)
          resp.splice(adminIndex, 1);
      this.usersSubject.next(resp);
      return this.users$;
    }))
  }

  login(loginUserDto : LoginUserDto)
  {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'No-Auth': 'True' });
    return this.http.post<LoggedUserDto>(loginUrl, loginUserDto, {headers: reqHeader}).pipe( 
      map(loggedUser => {
        if(loggedUser)
          {
            this.reloadPermissions();
            sessionStorage.setItem(tokenPath, JSON.stringify(loggedUser.token));
            this.loggedUserDto = loggedUser;
            return loggedUser;
          }
      })
    ); 
  }

  register(registerUserDto: RegisterUserDto){
    return this.http.post(registerUrl, registerUserDto, {responseType: 'text'}).subscribe( resp => {
      if (resp){
        this.users$.pipe(map(users => {
          users.push({username: registerUserDto.username, permissionLevelName: registerUserDto.permissionName});
          return users;
        })).subscribe();
        this.toastr.success(resp);
      }  
    });
  }

  updateMyPassword(changeCurrentUserPasswordDto: ChangeCurrentUserPasswordDto) {
      return this.http.post(updateCurrentUserPasswordUrl, changeCurrentUserPasswordDto, {responseType: 'text'})
  }

  deleteUser(username: string){
      this.http.delete(deleteUserUrl + username, {responseType: 'text'}).subscribe( resp =>{
        this.users$.pipe(map(users => {
          let user = users.find( (item) => item.username === username);
          let index = users.indexOf(user);
          users.splice(index,1);
          return users;
        })).subscribe();
        this.toastr.success(resp)
        return resp;
      });
  }

  updateUserPermission(simpleUserDto: SimpleUserDto){
      var request = this.http.post(updateUserPermissionUrl, simpleUserDto, {responseType: 'text'}).subscribe(resp => {
        this.users$.pipe(map(users => {
          let user = users.find( (item) => item.username === simpleUserDto.username);
          user.permissionLevelName = simpleUserDto.permissionLevelName;
          return users;
        })).subscribe();
        this.toastr.success(resp)
        return resp;
      })
  }

  isCurrentOrAdmin(username: string) {
    if(this.loggedUserDto.username == username || username == "Admin") 
      return true;
    else
      return false;
  }  
}
