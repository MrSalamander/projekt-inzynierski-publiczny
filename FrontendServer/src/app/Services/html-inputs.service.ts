import { HtmlInput } from './../Models/htmlInput';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getInputHtmlUrl, addInputHtmlUrl, updateInputHtmlUrl, deleteInputHtmlUrl } from 'src/assets/config';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HtmlInputsService {
  private _htmlInputs: HtmlInput[];

  constructor(private http: HttpClient) { }
  
  getHtmlInputByTarget(target: string): Promise<HtmlInput> {
    return this.getHtmlInputs().then(arr => {
      let index  = arr.findIndex(x => x.target == target);
      let item = arr[index]
      if(item) 
        return new Promise<HtmlInput>(resolve => { resolve(item)});
      else
        return null;
    })
  }
  
  getHtmlInputById(id: number): Promise<HtmlInput> {
    return this.getHtmlInputs().then(arr => {
      let index  = arr.findIndex(x => x.id == id);
      let item = arr[index]
      if(item) 
        return new Promise<HtmlInput>(resolve => { resolve(item)});
      else
        return null;
    })
  }

  private getHtmlInputs(): Promise<HtmlInput[]> {
    if(!this._htmlInputs)
        return this.initializeAndReturnHtmlInputs();
    return new Promise<HtmlInput[]>(resolve => resolve(this._htmlInputs));
  }

  addHtmlInput(htmlInput: HtmlInput): Observable<HtmlInput> {
    return this.http.post<number>(addInputHtmlUrl, htmlInput).pipe(
      map(resp => {
        htmlInput.id = resp;
        if(this._htmlInputs)
          this._htmlInputs.push(htmlInput);
        return htmlInput;
      })
    )
  }

  updateHtmlInput(htmlInput: HtmlInput) {
    return this.http.post(updateInputHtmlUrl, htmlInput, {responseType: 'text'})
    .pipe(
      map( resp => {
        if(this._htmlInputs){
          let index = this._htmlInputs.findIndex(x => x.id == htmlInput.id)
          let item = this._htmlInputs[index];
          item.target = htmlInput.target;
          item.value = htmlInput.value;
        }
      return htmlInput;
    }))
  }
  
  deleteHtmlInput(id: number) {
    return this.http.delete(deleteInputHtmlUrl+id, {responseType: 'text'}).pipe( 
      map( resp => {
        let index = this._htmlInputs.findIndex(x => x.id == id);
        this._htmlInputs.splice(index, 1);
        return resp;
      })
    )
  }

  private initializeAndReturnHtmlInputs() {
    return new Promise<HtmlInput[]>(resolve => {
      this.http.get<HtmlInput[]>(getInputHtmlUrl).subscribe(resp => { 
        this._htmlInputs = resp;
        return resolve(this._htmlInputs);
      })
    })
  }




}
