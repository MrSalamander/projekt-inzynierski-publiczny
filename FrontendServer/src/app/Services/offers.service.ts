import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SingleOfferTabContent } from './../Models/LocalUsageClasses/singleOfferTabContent';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getOffersUrl, addOfferUrl, updateOfferUrl, deleteOfferUrl, deleteOfferPhotoUrl } from 'src/assets/config';
import { Offer } from '../Models/offer';

@Injectable({
  providedIn: 'root'
})
export class OffersService {

  constructor(private http: HttpClient) { }

  getOffers() : Observable<SingleOfferTabContent[]>{
    return this.http.get<Offer[]>(getOffersUrl).pipe(map (resp => {
      let tabs: SingleOfferTabContent[] = []
      resp.forEach(offer => { 
        let tab =  this.MapOfferToTab(offer);
        tabs.push(tab); 
        tab.orderNumber = tabs.indexOf(tab);
       });
      return tabs;
    }))
  }

  addOffer(tab: SingleOfferTabContent) {
    let offer = this.MapTabToOffer(tab);
    return this.http.post<number>(addOfferUrl, offer);
  }

  updateOffer(tab: SingleOfferTabContent) {
    let offer = this.MapTabToOffer(tab);
    return this.http.post(updateOfferUrl, offer, {responseType: 'text'});
  }

  deleteOffer(offerId: number) {
    return this.http.delete(deleteOfferUrl + offerId, {responseType: 'text'});
  }

  deleteOfferPhoto(offerId){
    return this.http.delete(deleteOfferPhotoUrl + offerId, {responseType: 'text'});
  }
  private MapOfferToTab(offer: Offer): SingleOfferTabContent {
    let tab : SingleOfferTabContent= new SingleOfferTabContent();
    tab.id = offer.id;
    tab.orderNumber = offer.orderNumber;
    tab.htmlInput = offer.htmlInput;
    tab.image = offer.image;
    tab.title = offer.title;
    tab.target = offer.htmlInput?.target;
    tab.htmlPreview = false;
    tab.htmlPreviewValue = offer.htmlInput?.value;
    return tab;
  }

  private MapTabToOffer(tab: SingleOfferTabContent): Offer {
    let offer : Offer= new Offer();
    offer.id = tab.id;
    offer.orderNumber = tab.orderNumber;
    offer.htmlInput = tab.htmlInput;
    offer.image = tab.image;
    offer.title = tab.title;    
    return offer;
  }
}
