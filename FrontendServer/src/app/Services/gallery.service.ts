import { PhotoWithTargetDto } from './../Models/Photos/photo-with-target-dto';
import { PhotoDto } from './../Models/Photos/photo-dto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getGalleryAlbumsUrl, addGalleryAlbumUrl, updateGalleryAlbumUrl, deleteGalleryAlbumUrl, deletephotoUrl as deletePhotoUrl, addPhotoToAlbumUrl, getPhotosWithTarget_ExcludeGalleryAndWorkersUrl, addPhotoWithTarget as addPhotoWithTargetUrl } from 'src/assets/config';
import { PhotoAlbumReturnDto } from '../Models/Gallery/photo-album-return-dto';
import { PhotoAlbumAddDto } from '../Models/Gallery/photo-album-add-dto';
import { PhotoAlbumUpdateDto } from '../Models/Gallery/photo-album-update-dto';
import { PhotoFromAlbumDto } from '../Models/Photos/photo-from-album-dto';
import { ToastrService } from 'ngx-toastr';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  private _galleryAlbums : PhotoAlbumReturnDto[];
  private _photosWithTarget: PhotoWithTargetDto[];
  
  getAlbumsPromise(): Promise<PhotoAlbumReturnDto[]> {
    if(!this._galleryAlbums)
        return this.initializeAndReturnAlbums();
    return new Promise<PhotoAlbumReturnDto[]>(resolve => resolve(this._galleryAlbums));
  }

  getPhotosWithTargetPromise(): Promise<PhotoWithTargetDto[]> {
    if(!this._photosWithTarget)
        return this.initializeAndReturnPhotosWithTarget();
    return new Promise<PhotoWithTargetDto[]>(resolve => resolve(this._photosWithTarget));
  }

  constructor(private http: HttpClient, private toastr: ToastrService) { }

  addAlbum(album: PhotoAlbumAddDto) {
    this.http.post<PhotoAlbumReturnDto>(addGalleryAlbumUrl, album).subscribe( resp => {
        this._galleryAlbums.push(resp);
        this.toastr.success(`Pomyślnie dodano album ${resp.name}`);
      });
  }
      
  addPhotoToAlbum(newPhoto: FormData) {
    this.http.post<PhotoFromAlbumDto>(addPhotoToAlbumUrl, newPhoto).subscribe(newlyAddedPhoto => {
    var currentAlbum = this._galleryAlbums.find( x => x.id == newlyAddedPhoto.photoAlbumId);
    currentAlbum.photos.push(newlyAddedPhoto);
    this.toastr.success("Pomyślnie dodano zdjęcie do albumu");
    });
  }

  updateAlbum(album: PhotoAlbumUpdateDto){
    return this.http.post(updateGalleryAlbumUrl, album, {responseType: 'text'}).subscribe( resp => {
        this.toastr.success(resp);
      })
  }

  deleteAlbum(albumId: number){
    return this.http.delete(deleteGalleryAlbumUrl + albumId, {responseType: 'text'}).subscribe(resp =>
    { 
      let album = this._galleryAlbums.find( x => x.id === albumId)
      let index = this._galleryAlbums.indexOf(album);
      this._galleryAlbums.splice(index, 1);
      this.toastr.success(resp);
    });
  }

  deletePhotoFromAlbum(photo: PhotoFromAlbumDto){
    this.http.delete(deletePhotoUrl + photo.id, {responseType: 'text'}).subscribe( resp => {
      let album = this._galleryAlbums.find( x => x.id === photo.photoAlbumId)
      let index = album.photos.indexOf(photo);
      album.photos.splice(index, 1);
      this.toastr.success(resp);
    });
  }

  deletePhotoWithTarget(photo: PhotoWithTargetDto){
    return this.http.delete(deletePhotoUrl+ photo.id, {responseType: 'text'}).pipe( 
      tap(resp => {
        if(this._photosWithTarget != null){
          let index = this._photosWithTarget.indexOf(photo);
          this._photosWithTarget.splice(index, 1);
        }
      })
      );
  }

  addPhotoWithTarget(photo: FormData) {
    return this.http.post<PhotoWithTargetDto>(addPhotoWithTargetUrl, photo).pipe(
      tap((newlyAddedPhoto) => {
        if(this._photosWithTarget)
          this._photosWithTarget.push(newlyAddedPhoto)
      }))
  }
  
  private initializeAndReturnAlbums() {
    return new Promise<PhotoAlbumReturnDto[]>(resolve => 
      this.getAlbumsHttpRequest().subscribe(resp => { 
        this._galleryAlbums = resp;
        resolve(this._galleryAlbums);
      })
    )
  }

  private initializeAndReturnPhotosWithTarget() {
    return new Promise<PhotoWithTargetDto[]>(resolve => 
      this.getPhotosWithTargetHttpRequest().subscribe(resp => { 
        this._photosWithTarget = resp;
        resolve(this._photosWithTarget);
      })
    )
  }

  private getAlbumsHttpRequest() { 
    return this.http.get<PhotoAlbumReturnDto[]>(getGalleryAlbumsUrl);
  }

  private getPhotosWithTargetHttpRequest() { 
    return this.http.get<PhotoWithTargetDto[]>(getPhotosWithTarget_ExcludeGalleryAndWorkersUrl);
  }
}
