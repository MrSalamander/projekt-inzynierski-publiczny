import { ContactParam } from './../Models/Contact/contact-param';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { addContactParamUrl, deleteContactParamUrl, getAllContactsUrl, updateContactParamUrl } from 'src/assets/config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private _contactParams : ContactParam[];

  constructor(private http: HttpClient, private toastr: ToastrService) { }
  
  getContactParamsPromise(): Promise<ContactParam[]>{
      if(!this._contactParams)
      {
          return this.initializeContactParams();
      }
      return new Promise<ContactParam[]>(resolve => resolve(this._contactParams));
  }

  addContact(contactParam : ContactParam){
      this.insertContactParam(contactParam).subscribe(resp => {
          this._contactParams.push(resp);    
          this.toastr.success("Pomyślnie dodano parametr kontaktowy");  
      })
  }

  updateContact(contactParam : ContactParam){
      this.updateContactParam(contactParam).subscribe(resp => {
        let edited = this._contactParams.find( x => x.id == contactParam.id)  
        edited = resp;
        this.toastr.success("Pomyślnie zaktualizowano parametr kontaktowy");
      })
    }

  deleteContact(contactParam : ContactParam){
    this.deleteContactParam(contactParam.id).subscribe(resp => {
      let deletedIndex = this._contactParams.indexOf(contactParam);
      this._contactParams.splice(deletedIndex, 1);

      this.toastr.success(resp);
    })
  }

//#region  HttpRequests
  private getContactParams() { 
    return this.http.get<ContactParam[]>(getAllContactsUrl);
  }

  private insertContactParam(contactParam : ContactParam) {
    return this.http.post<ContactParam>(addContactParamUrl, {paramType: contactParam.paramType, paramValue: contactParam.paramValue});
  }

  private updateContactParam(contactParam : ContactParam) {
    return this.http.post<ContactParam>(updateContactParamUrl, contactParam);
  }

  private deleteContactParam(contactParamId: number) {
    return this.http.delete(deleteContactParamUrl + contactParamId, {responseType: 'text'});
  }
//#endregion

  private initializeContactParams() {
    return new Promise<ContactParam[]>(resolve => 
        this.getContactParams().subscribe(resp => { 
        this._contactParams = resp;
        resolve(this._contactParams);
    }))
}
}
