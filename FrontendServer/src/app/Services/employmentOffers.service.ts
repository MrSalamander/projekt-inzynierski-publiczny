import { EmploymentOfferDto } from '../Models/EmploymentOffers/employmentOfferDto';
import { addEmploymentOfferUrl, getAllEmploymentOffersUrl, deleteEmploymentOfferUrl, updateEmploymentOfferUrl, deleteEmploymentOfferPhotoUrl } from '../../assets/config';
import { GalleryService } from 'src/app/services/gallery.service';
import { EmploymentOffer } from '../Models/EmploymentOffers/employmentOffer';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { PhotoWithTargetDto } from '../Models/Photos/photo-with-target-dto';

@Injectable({
  providedIn: 'root'
})
export class EmploymentOffersService {
  public photos: PhotoWithTargetDto[] = [];

  constructor(private http: HttpClient, private galleryService: GalleryService) {
    this.galleryService.getPhotosWithTargetPromise().then(photos => this.photos = photos);
  }

  public getEmploymentOffers(){ 
    return this.http.get<EmploymentOfferDto[]>(getAllEmploymentOffersUrl).pipe<EmploymentOffer[]>(
      map( (collection) => {
        let offers: EmploymentOffer[] = [];
        collection.forEach(offerDto => {
          offers.push(
            this.mapToEntity(offerDto)  
          )
        });
        return offers;
      })
    );
  }

  public addEmploymentOffer(offer: EmploymentOffer) {
    let offerDto = this.mapToDto(offer);
    return this.http.post<EmploymentOfferDto>(addEmploymentOfferUrl, offerDto)
  }

  public updateEmploymentOffer(offer: EmploymentOffer) {
    let offerDto = this.mapToDto(offer);
    return this.http.post(updateEmploymentOfferUrl, offerDto, {responseType: 'text'})
  }

  public deleteEmploymentOffer(offer: EmploymentOffer) {
    return this.http.delete(deleteEmploymentOfferUrl + offer.id, {responseType: 'text'})
  }

  public deleteEmploymentOfferPhoto(offerId) {
    return this.http.delete(deleteEmploymentOfferPhotoUrl + offerId, {responseType: 'text'})
  }

  //#region privates
  private mapToDto(offer: EmploymentOffer) : EmploymentOfferDto {
    
    return {
      id: offer.id,
      photoId: offer.photo?.id,
      htmlInput: offer.htmlInput
    }
  }

  private mapToEntity(offer: EmploymentOfferDto) : EmploymentOffer{
    return {
      id: offer.id,
      photo: this.findImage(offer.photoId),
      htmlInput: offer.htmlInput
    }
  }

  private findImage(id: number) : PhotoWithTargetDto{
    return this.photos.find(x => x.id == id);
  }
  //#endregion
}
