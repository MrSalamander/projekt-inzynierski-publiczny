import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { addWorkerUrl, deleteWorkerPhotoUrl, deleteWorkerUrl, getAllWorkersUrl, updateWorkerUrl } from 'src/assets/config';
import { WorkerReturnDto } from '../Models/Workers/worker-return-dto';

@Injectable({
  providedIn: 'root'
})
export class WorkersService {
  private workersSubject = new ReplaySubject<WorkerReturnDto[]>()
  public workers$ = this.workersSubject.asObservable();
  

  constructor(private http: HttpClient, private toastr: ToastrService) { }

  getWorkers() {
    return this.http.get<WorkerReturnDto[]>(getAllWorkersUrl).pipe( 
      map(workers => {
        workers.forEach(worker => { worker.withUsFor = this.generateWithUsFrom(worker.workingFrom); })
        return workers;
    }))
  }
 
  addWorker(worker: FormData){
    return this.http.post<WorkerReturnDto>(addWorkerUrl, worker);
  }

  updateWorker(worker: FormData){
    return this.http.post<WorkerReturnDto>(updateWorkerUrl, worker).pipe(
      map(resp => {
        resp.withUsFor = this.generateWithUsFrom(resp.workingFrom);
        return resp;
      }));
  }

  deleteWorker(id: number){
      return this.http.delete(deleteWorkerUrl + id, {responseType: 'text'})
  }

  deleteWorkerPhoto(id: number){
    return this.http.delete(deleteWorkerPhotoUrl + id, {responseType: 'text'});
  }

  private generateWithUsFrom(workingFrom: Date): string {
    let currentTime = new Date();  
    let employmentDate = new Date(workingFrom);
    let difference = currentTime.getMonth() - employmentDate.getMonth() + 12*(currentTime.getFullYear() - employmentDate.getFullYear())

    let years : number = Math.floor(difference / 12);
    let months: number = difference % 12;
    let yearsText = "";
    if(years > 0) {
      if(years == 1) yearsText = "od roku ";
      else if( years >1 && years < 5) yearsText = years + " lata ";
      else yearsText = years + " lat ";
    }
    let monthsText = "";
    if(months > 0) {
      if(years > 0) monthsText = "i ";
      if(months == 1) monthsText += "1 miesiąc."; 
      else if( months >1 && months < 5) monthsText += months + " miesiące."; 
      else monthsText += months + " miesięcy."; 
    }
    return yearsText+monthsText;

  }
}
