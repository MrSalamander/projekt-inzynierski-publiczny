import { AuthAdminGuard } from './Guards/auth-admin-guard.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GalleryComponent } from './Components/gallery/gallery.component';
import { WorkersComponent } from './Components/workers/workers.component';
import { AdminPanelComponent } from './Components/admin-panel/admin-panel.component';
import { SharedModule } from './shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlbumCardComponent } from './Components/gallery/album-card/album-card.component';
import { OfferComponent } from './Components/offer/offer.component';
import { CalculatorComponent } from './Components/calculator/calculator.component';
import { RecruitmentComponent } from './Components/recruitment/recruitment.component';
import { CooperationComponent } from './Components/cooperation/cooperation.component';
import { ContactComponent } from './Components/contact/contact.component';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { LoginComponent } from './Components/admin-panel/login/login.component';
import { FormsModule } from '@angular/forms';
import { AuthorizationInterceptor } from './interceptors/authorization.interceptor';
import { HomePanelComponent } from './Components/home-panel/home-panel.component';
import { ContactParamComponent } from './Components/contact/contact-param/contact-param/contact-param.component';
import { PhotoUploadComponent } from './Components/gallery/photo-upload/photo-upload.component';
import { GalleryCarouselComponent } from './Components/gallery/album-card/gallery-carousel/gallery-carousel.component';
import { EmployOfferComponent } from './Components/recruitment/employ-offer/employ-offer.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HtmlCustomEditorComponent } from './Components/html-custom-editor/html-custom-editor.component';
import { UnsafeHtmlPipe } from 'src/app/Guards/UnsafeHtmlPipe';

@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    WorkersComponent,
    AdminPanelComponent,
    AlbumCardComponent,
    OfferComponent,
    CalculatorComponent,
    RecruitmentComponent,
    CooperationComponent,
    ContactComponent,
    LoginComponent,
    HomePanelComponent,
    ContactParamComponent,
    PhotoUploadComponent,
    GalleryCarouselComponent,
    EmployOfferComponent,
    HtmlCustomEditorComponent,
    UnsafeHtmlPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    SharedModule,
    FormsModule,
    AngularEditorModule
  ],
  providers: [
    AuthAdminGuard,
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
