import { Component, HostListener, ViewChild } from "@angular/core";
import { UsersService } from "./services/users.service";
import { permissionLevelAdmin } from "./StaticParams/permissions";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
constructor(public usersService: UsersService){}

  title = 'Aleksander Fedyczek - praca inżynierska';
  adminPermissionLevel = permissionLevelAdmin;
  
  @HostListener("window:onbeforeunload",["$event"])
    clearLocalStorage(event){
        localStorage.clear();
    }




}
