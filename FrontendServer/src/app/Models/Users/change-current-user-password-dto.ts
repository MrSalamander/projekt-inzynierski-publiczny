export class ChangeCurrentUserPasswordDto{
    currentPassword : string;
    newPassword : string;
}