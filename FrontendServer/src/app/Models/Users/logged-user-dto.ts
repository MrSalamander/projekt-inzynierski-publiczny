export class LoggedUserDto{
    username: string;
    token: string;
    permissionLevelName: string;
}