import { HtmlInput } from '../htmlInput';
import { PhotoWithTargetDto } from '../Photos/photo-with-target-dto';
export class EmploymentOffer {
    id: number;
    photo: PhotoWithTargetDto;
    htmlInput: HtmlInput;
}