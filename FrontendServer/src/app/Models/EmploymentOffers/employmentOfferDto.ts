import { HtmlInput } from '../htmlInput';
export class EmploymentOfferDto {
    id: number;
    photoId: number;
    htmlInput: HtmlInput;
}