import { PhotoDto } from "../Photos/photo-dto";

export class WorkerReturnDto{
    id: number;
    name: string;
    rank: string;
    workingFrom: Date;
    withUsFor: string;
    profilePhoto: PhotoDto;
}