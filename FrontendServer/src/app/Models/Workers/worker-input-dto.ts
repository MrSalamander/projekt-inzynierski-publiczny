export class WorkerInputDto{
    id: number;
    name: string;
    rank: string;
    workingFrom: Date;
    photo: File;
}