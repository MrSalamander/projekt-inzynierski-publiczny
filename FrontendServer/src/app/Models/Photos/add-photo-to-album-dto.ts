export class AddPhotoToAlbumDto {
    file: File;
    comment: string;
    albumId: number;
}