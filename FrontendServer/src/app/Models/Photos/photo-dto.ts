export class PhotoDto{
    id: number;
    path: string;
    comment: string;
}