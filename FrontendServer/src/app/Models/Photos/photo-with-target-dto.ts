import { PhotoDto } from "./photo-dto";

export class PhotoWithTargetDto extends PhotoDto{
    photoTarget: string;
}