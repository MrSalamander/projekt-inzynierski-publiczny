import { PhotoDto } from "./photo-dto";

export class PhotoFromAlbumDto extends PhotoDto{
    photoAlbumId: number;
    thumbnailPath: string;
}