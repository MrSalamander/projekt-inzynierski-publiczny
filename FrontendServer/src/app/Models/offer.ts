import { HtmlInput } from './htmlInput';
import { PhotoWithTargetDto } from './Photos/photo-with-target-dto';
export class Offer{
    id: number;
    orderNumber: number
    title: string;
    image: PhotoWithTargetDto
    htmlInput: HtmlInput;
}