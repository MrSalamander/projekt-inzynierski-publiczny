import { HtmlInput } from "../htmlInput";
import { PhotoWithTargetDto } from "../Photos/photo-with-target-dto";

export class ImageAndHtmlPair {
    image: PhotoWithTargetDto;

    htmlInput: HtmlInput;
    htmlPreview: boolean;
    htmlPreviewValue: string;

    target: string;
}