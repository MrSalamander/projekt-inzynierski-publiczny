import { Offer } from './../offer';
import { HtmlInput } from "../htmlInput";
import { PhotoWithTargetDto } from "../Photos/photo-with-target-dto";

export class SingleOfferTabContent extends Offer{
    htmlInput: HtmlInput;
    htmlPreview: boolean;
    htmlPreviewValue: string;
    target: string;
}