import { PhotoFromAlbumDto } from "../Photos/photo-from-album-dto";

export class PhotoAlbumReturnDto{
    id: number;
    name: string;
    comment: string;

    photos : PhotoFromAlbumDto[] = [];
}