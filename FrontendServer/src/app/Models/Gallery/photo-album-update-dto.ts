export class PhotoAlbumUpdateDto{
    id : Number;
    name : string;
    comment : string;
}