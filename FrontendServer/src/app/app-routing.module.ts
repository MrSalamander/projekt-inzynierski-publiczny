import { AuthAdminGuard } from './Guards/auth-admin-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPanelComponent } from './Components/admin-panel/admin-panel.component';
import { LoginComponent } from './Components/admin-panel/login/login.component';
import { ContactComponent } from './Components/contact/contact.component';
import { CooperationComponent } from './Components/cooperation/cooperation.component';
import { GalleryComponent } from './Components/gallery/gallery.component';
import { HomePanelComponent } from './Components/home-panel/home-panel.component';
import { OfferComponent } from './Components/offer/offer.component';
import { RecruitmentComponent } from './Components/recruitment/recruitment.component';


const routes: Routes = [
  {path: 'offer', component: OfferComponent},
  {path: 'gallery', component: GalleryComponent},
  {path: 'recruitment', component: RecruitmentComponent},
  {path: 'cooperation', component: CooperationComponent},
  {path: 'info', component: ContactComponent},
  {path: 'admin-panel', component: AdminPanelComponent, canActivate: [AuthAdminGuard]},
  {path: 'login', component: LoginComponent},
  {path: '**', component: HomePanelComponent, pathMatch: 'full'},
  {path: '', component: HomePanelComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
