import { AngularEditorConfig } from "@kolkov/angular-editor";

export const ContactParamEmail = 'email';
export const ContactParamPhone = 'phone';
export const ContactParamFacebook = 'facebook';

export const ContactParamTypes = [ContactParamEmail, ContactParamPhone, ContactParamFacebook]
