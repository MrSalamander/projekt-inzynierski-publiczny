export const permissionLevelAdmin = 'Admin';
export const permissionLevelStarszyEdytor = 'Starszy edytor';
export const permissionLevelMlodszyEdytor = 'Młodszy edytor';

export const permissions = [permissionLevelAdmin, permissionLevelStarszyEdytor, permissionLevelMlodszyEdytor];
