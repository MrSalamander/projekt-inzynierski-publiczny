const baseUrl = 'https://localhost:5001';
const apiUrl = baseUrl + '/api';

// gallery.service
const galleryUrl = `${apiUrl}/gallery`;
export const getGalleryAlbumsUrl = galleryUrl;
export const getPhotosWithTarget_ExcludeGalleryAndWorkersUrl = `${galleryUrl}/PhotosWithTarget_ExcludeGalleryAndWorkers`;
export const addGalleryAlbumUrl = `${galleryUrl}/addAlbum`;
export const addPhotoToAlbumUrl = `${galleryUrl}/addPhotoToAlbum`;
export const addPhotoWithTarget = `${galleryUrl}/addPhotoWithTarget`;
export const updateGalleryAlbumUrl = `${galleryUrl}/updateAlbum`;
export const deleteGalleryAlbumUrl = `${galleryUrl}/deleteAlbum/`;
export const deletephotoUrl = `${galleryUrl}/delete-photo/`;
export const photosUrl = `${galleryUrl}/`;

// users.service
const usersUrl = `${apiUrl}/users`;
export const loginUrl = `${usersUrl}/login`;
export const getUsersUrl = `${usersUrl}/`;
export const updateCurrentUserPasswordUrl = `${usersUrl}/update-my-password`;
export const deleteUserUrl = `${usersUrl}/delete/`;
export const updateUserPermissionUrl = `${usersUrl}/update-permission`;
export const registerUrl= `${usersUrl}/register`;

// workers.service
const workersUrl = `${apiUrl}/workers`;
export const getAllWorkersUrl = `${workersUrl}`;
export const addWorkerUrl = `${workersUrl}/add`;
export const updateWorkerUrl = `${workersUrl}/update`;
export const deleteWorkerUrl = `${workersUrl}/delete/`;
export const deleteWorkerPhotoUrl = `${workersUrl}/delete-photo/`;

// contact.service
const contactUrl = `${apiUrl}/contact`;
export const getAllContactsUrl = `${contactUrl}`;
export const addContactParamUrl = `${contactUrl}`;
export const updateContactParamUrl = `${contactUrl}/update`;
export const deleteContactParamUrl = `${contactUrl}/delete/`;

// authorization
export const tokenPath = `authorization/token`;

// employmentOffers.service
const employmentOffersUrl = `${apiUrl}/employmentOffers`;
export const getAllEmploymentOffersUrl = employmentOffersUrl;
export const addEmploymentOfferUrl = employmentOffersUrl;
export const updateEmploymentOfferUrl = `${employmentOffersUrl}/update`;
export const deleteEmploymentOfferUrl = `${employmentOffersUrl}/delete/`;
export const deleteEmploymentOfferPhotoUrl = `${employmentOffersUrl}/deleteOfferPhoto/`;

// htmlInputs.service
const htmlInputsUrl = `${apiUrl}/htmlInputs`;
export const getInputHtmlUrl = `${htmlInputsUrl}`;
export const addInputHtmlUrl = `${htmlInputsUrl}/add`;
export const updateInputHtmlUrl = `${htmlInputsUrl}/update`;
export const deleteInputHtmlUrl = `${htmlInputsUrl}/delete/`;

// offers.service
const offersUrl = `${apiUrl}/offers`;
export const getOffersUrl = `${offersUrl}`;
export const addOfferUrl = `${offersUrl}/add`;
export const updateOfferUrl = `${offersUrl}/update`;
export const deleteOfferUrl = `${offersUrl}/delete/`;
export const deleteOfferPhotoUrl = `${offersUrl}deleteOfferPhoto/`

